<?php
session_start();
// Allow only admins to access this page.
if($_SESSION['userType'] != 1){
  die();
}

$DATABASE_HOST = 'localhost';
$DATABASE_USER = 'root';
$DATABASE_PASS = '';
$DATABASE_NAME = 'freshman';

$has_errors = 0;
$ERROR ="" ;

$con = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);

if ( !$con ) {
 
  echo 'Unable to connect with database ';
}

else{


//ADDING STUDENT IN DATABASE
  if(isset($_POST['add_student'])){
    
    $uname=$_POST['username'];
    $firstname=$_POST['firstname'];
    $lastname=$_POST['lastname'];
    $email=$_POST['email'];
    $phone=$_POST['phone'];
    $password=$_POST['password'];
    $espb=$_POST['espb'];
    //$cash=$_POST['cash'];
    $confirm_password=$_POST['confirm_password'];
    $index=$_POST['student_index'];
    $parent=$_POST['parent_name'];
    $status=$_POST['study_status'];
    $date_of_entry=$_POST['date_of_entry'];
    $jmbg=$_POST['jmbg'];
    $year=$_POST['year']; 
    $course_id=$_POST['course_id'];
   
    if($password!=$confirm_password){
      die("Passwords doesn't match");
    }

    $sql1="select * from fr_users where USERNAME='".$uname."' ";
    
    $result=mysqli_query($con,$sql1);
    $resrows=mysqli_num_rows( $result);

    if( $resrows > 0){
        //$ERROR="User with username already exist";$has_errors = 1;
        die("User with this username already exist");
      }

    $sql2="select * from fr_users where EMAIL='".$email."' ";
    
    $result=mysqli_query($con,$sql2);
    $resrows=mysqli_num_rows( $result);

    if( $resrows > 0){
         // $ERROR="User with email already exist";$has_errors = 1;
         die("User with this email already exist");
      }
    $sql3="select * from fr_users where JMBG='".$jmbg."' ";
    
    $result=mysqli_query($con,$sql3);
    $resrows=mysqli_num_rows( $result);

    if( $resrows > 0){
         // $ERROR="User with email already exist";$has_errors = 1;
           die("User with this JMBG already exist");
    }
    $sql4="select * from fr_users where STUDENT_INDEX='".$index."' ";
    
        $result=mysqli_query($con,$sql4);
        $resrows=mysqli_num_rows( $result);

        if( $resrows > 0){
         // $ERROR="User with email already exist";$has_errors = 1;
            die("Student with this index already exist");
      }

          $sql="INSERT INTO fr_users (USERNAME, FIRSTNAME, LASTNAME, EMAIL, PASSWORD, PHONE_NUMBER, ESPB, USER_TYPE, BALANCE, COURSE_ID,STUDENT_INDEX,PARENT_NAME, JMBG, STUDY_STATUS, DATE_OF_ENTRY, YEAR) VALUES ('$uname','$firstname','$lastname','$email','$password','$phone','$espb',3,0,'$course_id','$index','$parent','$jmbg','$status','$date_of_entry','$year')";
    
          if(!mysqli_query($con,$sql)){
              die("Error inserting new record");
           }
        $has_errors = 0;
    }

//ADDING EXAM IN DATABASE
  if(isset($_POST['add_course'])){
    
    $name=$_POST['name'];   
    $espb=$_POST['espb'];
    $mandatory=$_POST['mandatory'];
    

    $sql1="select * from fr_exams where EXAM_NAME='".$name."' ";
    
      $result=mysqli_query($con,$sql1);
      $resrows=mysqli_num_rows( $result);

      if( $resrows > 0){
        //$ERROR="Course with this name already exist";
        //$has_errors = 1;
        die("Course with this name already exist");

      }
     $sql_exam="INSERT INTO `fr_exams` (`EXAM_NAME`, `ESPB`, `MANDATORY`) VALUES ('$name', '$espb', '$mandatory')";
    
       if(!mysqli_query($con,$sql_exam)){
         die("Error inserting new record");
        }
        $has_errors = 0;
    }

    //ADDING EXAMS TO COURSE IN DATABASE
  if(isset($_POST['add_exam_course'])){
    
    $course=$_POST['course_id'];   
    $exam=$_POST['exam_id'];
    $semester=$_POST['semester'];
    

    $sql1="select * from fr_exams where EXAM_ID='".$exam."' ";
    
      $result=mysqli_query($con,$sql1);
      $resrows=mysqli_num_rows( $result);

      if( $resrows <= 0){
        //$ERROR="Course with this name already exist";
        //$has_errors = 1;
        die("Exam with this id doesn't exist");

      }
      $sql2="select * from fr_courses where COURSE_ID='".$course."' ";
    
      $result1=mysqli_query($con,$sql2);
      $resrows1=mysqli_num_rows( $result1);

      if( $resrows1 <= 0){
        //$ERROR="Course with this name already exist";
        //$has_errors = 1;
        die("Course with this id doesn't exist");

      }
      $sql3="select * from fr_exams_courses where COURSE_ID='".$course."' and  EXAM_ID='".$exam."'";
    
      $result3=mysqli_query($con,$sql3);
      $resrows3=mysqli_num_rows( $result3);

      if( $resrows3 > 0){
        //$ERROR="Course with this name already exist";
        //$has_errors = 1;
        die("Already exists in database");

      }
     $sql_exam="INSERT INTO `fr_exams_courses` (`COURSE_ID`, `EXAM_ID`, `SEMESTER`) VALUES ('$course', '$exam', '$semester')";
    
       if(!mysqli_query($con,$sql_exam)){
         die("Error inserting new record");
        }
        $has_errors = 0;
    }
//ADDING PROFESSOR IN DATABASE
  if(isset($_POST['add_professor'])){
    
    $uname=$_POST['username'];
    $firstname=$_POST['firstname'];
    $lastname=$_POST['lastname'];
    $email=$_POST['email'];
    $phone=$_POST['phone'];
    $password=$_POST['password'];
    $confirm_password=$_POST['confirm_password'];
    $index="0";
    $parent=$_POST['parent_name'];
    $status="0";
    $date_of_entry="1111-11-11";
    $jmbg=$_POST['jmbg'];


    if($password!=$confirm_password){
      die("Passwords doesn't match");
    }
    $sql1="select * from fr_users where USERNAME='".$uname."' ";
    
    $result=mysqli_query($con,$sql1);
    $resrows=mysqli_num_rows( $result);

    if( $resrows > 0){
        //$ERROR="User with username already exist";$has_errors = 1;
        die("User with this username already exist");
      }
     
      $sql2="select * from fr_users where EMAIL='".$email."' ";
    
      $result=mysqli_query($con,$sql2);
      $resrows=mysqli_num_rows( $result);

      if( $resrows > 0){
         // $ERROR="User with email already exist";$has_errors = 1;
            die("User with this email already exist");
      }
  
     $sql3="select * from fr_users where JMBG='".$jmbg."' ";
    
        $result=mysqli_query($con,$sql3);
        $resrows=mysqli_num_rows( $result);

        if( $resrows > 0){
         // $ERROR="User with email already exist";$has_errors = 1;
            die("User with this JMBG already exist");
        }
    $sql="INSERT INTO fr_users (USERNAME, FIRSTNAME, LASTNAME, EMAIL, PASSWORD, PHONE_NUMBER, ESPB, USER_TYPE, BALANCE,STUDENT_INDEX,PARENT_NAME, JMBG, STUDY_STATUS, DATE_OF_ENTRY,YEAR) VALUES ('$uname','$firstname','$lastname','$email','$password','$phone',0,2,0,'$index','$parent','$jmbg','$status','$date_of_entry',0)";
    
       if(!mysqli_query($con,$sql)){
         die("Error inserting new record");
        }
        $has_errors = 0;
   }

//ADDING ADMINISTRATIVE EMPLOYEE IN DATABASE
  if(isset($_POST['add_employee'])){
    
    $uname=$_POST['username'];
    $firstname=$_POST['firstname'];
    $lastname=$_POST['lastname'];
    $email=$_POST['email'];
    $phone=$_POST['phone'];
    $password=$_POST['password'];
    $confirm_password=$_POST['confirm_password'];
    $index="0";
    $parent=$_POST['parent_name'];
    $status="0";
    $date_of_entry="1111-11-11";
    $jmbg=$_POST['jmbg'];

    if($password!=$confirm_password){
      die("Passwords doesn't match");
    }
   
      $sql1="select * from fr_users where USERNAME='".$uname."' ";
    
      $result=mysqli_query($con,$sql1);
      $resrows=mysqli_num_rows( $result);

      if( $resrows > 0){
        //$ERROR="User with username already exist";$has_errors = 1;
        die("User with this username already exist");

      }
    
        $sql2="select * from fr_users where EMAIL='".$email."' ";
    
        $result=mysqli_query($con,$sql2);
        $resrows=mysqli_num_rows( $result);

        if( $resrows > 0){
         // $ERROR="User with email already exist";$has_errors = 1;
            die("User with this email already exist");
      }
       $sql3="select * from fr_users where JMBG='".$jmbg."' ";
    
        $result=mysqli_query($con,$sql3);
        $resrows=mysqli_num_rows( $result);

        if( $resrows > 0){
         // $ERROR="User with email already exist";$has_errors = 1;
            die("User with this JMBG already exist");
      }
       
  $sql="INSERT INTO fr_users (USERNAME, FIRSTNAME, LASTNAME, EMAIL, PASSWORD, PHONE_NUMBER, ESPB, USER_TYPE, BALANCE, STUDENT_INDEX,PARENT_NAME, JMBG, STUDY_STATUS, DATE_OF_ENTRY,YEAR) VALUES ('$uname','$firstname','$lastname','$email','$password','$phone',0,4,0, '$index','$parent','$jmbg','$status','$date_of_entry',0)";
        
       if(!mysqli_query($con,$sql)){
         die("Error inserting new record");
        }
        $has_errors = 0;
 }
 //ADDING COURSE IN DATABASE
  if(isset($_POST['add_exam'])){
    
    $name=$_POST['name'];   
    $espb=$_POST['espb'];
    

    $sql1="select * from fr_courses where COURSE_NAME='".$name."' ";
    
      $result=mysqli_query($con,$sql1);
      $resrows=mysqli_num_rows( $result);

      if( $resrows > 0){
        //$ERROR="Course with this name already exist";
        //$has_errors = 1;
        die("Course with this name already exist");

      }
     $sql_exam="INSERT INTO `fr_courses` (`COURSE_NAME`, `COURSE_ESPB`) VALUES ('$name', '$espb')";
    
       if(!mysqli_query($con,$sql_exam)){
         die("Error inserting new record");
        }
        $has_errors = 0;
    }
//DELETING USERS
if (isset($_GET['del'])) {
  $id = $_GET['del'];
  mysqli_query($con, "DELETE FROM fr_users WHERE ID=$id");
  $_SESSION['message_del'] = "User deleted!"; 

}
//DELETING EXAMS
if (isset($_GET['del_course'])) {
  $id = $_GET['del_course'];
  mysqli_query($con, "DELETE FROM fr_exams WHERE EXAM_ID=$id");
  $_SESSION['message_del'] = "Course deleted!"; 
 
}
//DELETING COURSES
if (isset($_GET['del_exam'])) {
  $id = $_GET['del_exam'];
  mysqli_query($con, "DELETE FROM fr_courses WHERE COURSE_ID=$id");
  $_SESSION['message_del'] = "Course deleted!"; 
 
}
//DELETING EXAMS FOR COURSES
if (isset($_GET['del_exam_courses'])) {
  $id = $_GET['del_exam_courses'];
  mysqli_query($con, "DELETE FROM fr_exams_courses WHERE EXAMS_COURSES_ID=$id");
  $_SESSION['message_del'] = "Exam deleted!"; 
 
}
}?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Admin</title>
    <link rel="stylesheet" href="../css/admin1234.css">
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
  </head>
  <body>
    
<nav class="sidebar">
      <div class="text">Menu</div>
<ul>
<li>
          <a href="#" class="serv-btn1">Student
            <span class="fas fa-caret-down first" style="
    padding: 0;
"></span>
          </a>
          <ul class="serv-show1">
            <li class="addStudent"><a >Add new</a></li>
            <li class="viewStudent"><a href="#">View all</a></li>
          </ul>
</li>
<li>
          <a href="#" class="serv-btn2">Professor
            <span class="fas fa-caret-down second" style="
    padding: 0;">
      
    </span>
          </a>
          <ul class="serv-show2">
            <li class="addProfessor"><a href="#">Add new</a></li>
            <li class="viewProfessor"><a href="#">View all</a></li>
          </ul>
</li>
<li>
          <a href="#" class="serv-btn3">Adm employee
            <span class="fas fa-caret-down third" style="
    padding: 0;
"></span>
          </a>
          <ul class="serv-show3">
            <li class="addEmployee"><a href="#">Add new</a></li>
            <li class="viewEmployee"><a href="#">View all</a></li>
          </ul>
</li>
<li>
          <a href="#" class="serv-btn4">Exam
            <span class="fas fa-caret-down fourth" style="
    padding: 0;
"></span>
          </a>
          <ul class="serv-show4">
            <li class="addCourse"><a href="#">Add new</a></li>
            <li class="viewCourse"><a href="#">View all</a></li>
          </ul>
</li>
<li>
          <a href="#" class="serv-btn5">Course
            <span class="fas fa-caret-down fifth" style="
    padding: 0;
"></span>
          </a>
          <ul class="serv-show5">
            <li class="addExam"><a href="#">Add new</a></li>
            <li class="viewExam"><a href="#">View all</a></li>
            <li class="connectExams"><a href="#">Add Exam</a></li>
            <li class="viewConnectExams"><a href="#">View Exams</a></li>
            
          </ul>
</li>

<li>
          <a href="logout.php" >Log out </a>
          
</li>

</ul>
</nav>

<!--FOR ADDING STUDENT-->

    <div class="content1">
      <div class="login">
      <div class="logo">
        <img src="../images/logo.svg" style="width: 18%;">
      </div>
      <h1>Add student</h1>
      <p class="error" <?php if($has_errors) {echo 'style="border:1.5px solid red;"';} ?>><?php if($has_errors) {echo $ERROR ;} ?></p>
      <form method="post">
        <div class="grid">

        <div class= "label">
          <label for="firstname">
            <i class="fas fa-user"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="firstname" placeholder="Firstname"  required>
         </div> 
          
        <div class= "label">
          <label for="lastname">
            <i class="fas fa-user"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="lastname" placeholder="Lastname"  required>
         </div> 
          
         <div class= "label">
          <label for="email">
            <i class="fas fa-envelope"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="email" placeholder="Email"  required>
         </div> 

         <div class= "label">
          <label for="phone">
            <i class="fas fa-phone"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="phone" placeholder="Phone number"  required>
         </div> 
         <div class= "label">
          <label for="jmbg">
            <i class="fas fa-address-card"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="jmbg" placeholder="JMBG"  required>
         </div> 
         <div class= "label">
          <label for="parent">
            <i class="fas fa-user-circle"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="parent_name" placeholder="Name of parent"  required>
         </div> 
        <div class= "label">
          <label for="student_index">
            <i class="fas fa-info"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="student_index" placeholder="Student index"  required>
         </div> 
         
         <div class= "label">
          <label for="study_status">
            <i class="fas fa-info-circle"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="study_status" placeholder="Study status"  required>
         </div> 
         
         <div class= "label">
          <label for="date_of_entry">
            <i class="fas fa-calendar"></i>
          </label>
        </div>
        <div class="input">
          <input type="date" name="date_of_entry"   required>
         </div> 

         <div class= "label">
          <label for="espb">
            <i class="fas fa-graduation-cap"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="espb" placeholder="ESPB"  required>
         </div> 

          <div class= "label">
          <label for="year">
            <i class="fas fa-graduation-cap"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="year" placeholder="YEAR"  required>
         </div> 

         <div class= "label">
          <label for="course">
            <i class="fa fa-credit-card" aria-hidden="true"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="course_id" placeholder="Course"  required>
         </div> 

        <div class= "label">
          <label for="username">
            <i class="fas fa-user"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="username" placeholder="Username"  required>
         </div> 

         <div class= "label">
          <label for="password">
            <i class="fas fa-lock"></i>
          </label>
        </div>
        <div class="input">
        <input type="password" name="password" placeholder="Password"  required>
      </div>

      <div class= "label">
          <label for="password">
            <i class="fas fa-lock"></i>
          </label>
        </div>
        <div class="input">
        <input type="password" name="confirm_password" placeholder="Confirm password"  required>
      </div>
      </div>

        <input type="submit" name="add_student" value="Add">

      </form></div></div>


<!--FOR ADDING EXAM-->

<div class="content2">
      <div class="login">
      <div class="logo">
        <img src="../images/logo.svg" style="width: 30%;">
      </div>
      <h1>Add exam</h1>
      <p class="error" <?php if($has_errors) {echo 'style="border:1.5px solid red;"';} ?>><?php if($has_errors) {echo $ERROR ;} ?></p>
      <form method="post">
        <div class="grid">

        <div class= "label">
          <label for="name">
            <i class="fas fa-graduation-cap"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="name" placeholder="Name of exam"  required>
         </div> 
            
 <div class= "label">
          <label for="espb">
            <i class="fas fa-book"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="espb" placeholder ="ESPB"  required>
         </div>  
        
</div>
          <div class="radio_btn">
         <h3 for="mandatory">
            Mandatory:
          </h3>
        
        
          <input type="radio" name="mandatory" value="1" >Yes</input>
          <input type="radio" name="mandatory" value="0" checked style=" margin-left: 5%;">No</input>
         </div> 
          
        <input type="submit" name="add_course" value="Add">

      </form>
    </div>
</div>

<!--FOR ADDING COURSE-->

<div class="content9">
      <div class="login">
      <div class="logo">
        <img src="../images/logo.svg" style="width: 30%;">
      </div>
      <h1>Add course</h1>
      <p class="error" <?php if($has_errors) {echo 'style="border:1.5px solid red;"';} ?>><?php if($has_errors) {echo $ERROR ;} ?></p>
      <form method="post">
        <div class="grid">

        <div class= "label">
          <label for="name">
            <i class="fas fa-graduation-cap"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="name" placeholder="Name of course"  required>
         </div> 
            
 <div class= "label">
          <label for="espb">
            <i class="fas fa-book"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="espb" placeholder ="ESPB"  required>
         </div>  
        
</div>
          
          
        <input type="submit" name="add_exam" value="Add">

      </form>
    </div>
</div>


<!--FOR ADDING EXAMS FOR COURSE-->

<div class="content11">
      <div class="login">
      <div class="logo">
        <img src="../images/logo.svg" style="width: 30%;">
      </div>
      <h1>Add exams to course</h1>
      <p class="error" <?php if($has_errors) {echo 'style="border:1.5px solid red;"';} ?>><?php if($has_errors) {echo $ERROR ;} ?></p>
      <form method="post">
        <div class="grid">

        <div class= "label">
          <label for="course">
            <i class="fas fa-graduation-cap"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="course_id" placeholder="COURSE ID"  required>
         </div> 
            
        <div class= "label">
          <label for="exam">
            <i class="fas fa-book"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="exam_id" placeholder ="EXAM ID"  required>
         </div>

          <div class= "label">
          <label for="semester">
            <i class="fas fa-credit-card"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="semester" placeholder ="SEMESTER"  required>
         </div>  
        
</div>
          
          
        <input type="submit" name="add_exam_course" value="Add">

      </form>
    </div>
</div>





<!--FOR ADDING PROFESSOR-->

    <div class="content3">
      <div class="login">
      <div class="logo">
        <img src="../images/logo.svg" style="width: 30%;">
      </div>
      <h1>Add professor</h1>
      <p class="error" <?php if($has_errors) {echo 'style="border:1.5px solid red;"';} ?>><?php if($has_errors) {echo $ERROR ;} ?></p>
      <form method="post">
        <div class="grid">

        <div class= "label">
          <label for="firstname">
            <i class="fas fa-user"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="firstname" placeholder="Firstname"  required>
         </div> 
          
        <div class= "label">
          <label for="lastname">
            <i class="fas fa-user"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="lastname" placeholder="Lastname"  required>
         </div> 
          
         <div class= "label">
          <label for="email">
            <i class="fas fa-envelope"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="email" placeholder="Email"  required>
         </div> 

         <div class= "label">
          <label for="phone">
            <i class="fas fa-phone"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="phone" placeholder="Phone number"  required>
         </div> 

<div class= "label">
          <label for="jmbg">
            <i class="fas fa-address-card"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="jmbg" placeholder="JMBG"  required>
         </div> 
         <div class= "label">
          <label for="parent">
            <i class="fas fa-user-circle"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="parent_name" placeholder="Name of parent"  required>
         </div> 
        
        <div class= "label">
          <label for="username">
            <i class="fas fa-user"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="username" placeholder="Username"  required>
         </div> 

         <div class= "label">
          <label for="password">
            <i class="fas fa-lock"></i>
          </label>
        </div>
        <div class="input">
        <input type="password" name="password" placeholder="Password"  required>
      </div>

      <div class= "label">
          <label for="password">
            <i class="fas fa-lock"></i>
          </label>
        </div>
        <div class="input">
        <input type="password" name="confirm_password" placeholder="Confirm password"  required>
      </div>
      </div>

        <input type="submit" name="add_professor" value="Add">

      </form></div></div>

<!--FOR ADDING ADMINISTRATIVE EMPLOYEE-->

    <div class="content4">
      <div class="login">
      <div class="logo">
        <img src="../images/logo.svg" style="width: 30%;">
      </div>
      <h1>Add administrative employee</h1>
      <p class="error" <?php if($has_errors) {echo 'style="border:1.5px solid red;"';} ?>><?php if($has_errors) {echo $ERROR ;} ?></p>
      <form method="post">
        <div class="grid">

        <div class= "label">
          <label for="firstname">
            <i class="fas fa-user"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="firstname" placeholder="Firstname"  required>
         </div> 
          
        <div class= "label">
          <label for="lastname">
            <i class="fas fa-user"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="lastname" placeholder="Lastname"  required>
         </div> 
          
         <div class= "label">
          <label for="email">
            <i class="fas fa-envelope"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="email" placeholder="Email"  required>
         </div> 

         <div class= "label">
          <label for="phone">
            <i class="fas fa-phone"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="phone" placeholder="Phone number"  required>
         </div> 

         <div class= "label">
          <label for="jmbg">
            <i class="fas fa-address-card"></i>
          </label>
        </div>
         <div class="input">
          <input type="text" name="jmbg" placeholder="JMBG"  required>
         </div> 

         <div class= "label">
          <label for="parent">
            <i class="fas fa-user-circle"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="parent_name" placeholder="Name of parent"  required>
         </div> 
      

        <div class= "label">
          <label for="username">
            <i class="fas fa-user"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="username" placeholder="Username"  required>
         </div> 

         <div class= "label">
          <label for="password">
            <i class="fas fa-lock"></i>
          </label>
        </div>
        <div class="input">
        <input type="password" name="password" placeholder="Password"  required>
      </div>

      <div class= "label">
          <label for="password">
            <i class="fas fa-lock"></i>
          </label>
        </div>
        <div class="input">
        <input type="password" name="confirm_password" placeholder="Confirm password"  required>
      </div>
      </div>

        <input type="submit" name="add_employee" value="Add">

      </form></div></div>


<!--FOR VIEWING STUDENTS-->

    <div class="content5">
     <?php $results = mysqli_query($con, "SELECT * FROM fr_users where USER_TYPE=3 "); ?>

<table>
  <thead>
    <tr>
      <th>First name</th>
      <th>Parent</th>
      <th>Last name</th>
      <th>Username</th>
      <th>Email</th>
      <th>Phone</th>
      <th>Espb</th>
      <th>Balance</th>
      <th>Index</th>
      <th>JMBG</th>
      <th>Study status</th>
      <th colspan="2">Action</th>
    </tr>
  </thead>
  
  <?php while ($row = mysqli_fetch_array($results)) { ?>
    <tr>
      <td><?php echo $row['FIRSTNAME']; ?></td>
      <td><?php echo $row['PARENT_NAME']; ?></td>
      <td><?php echo $row['LASTNAME']; ?></td>
      <td><?php echo $row['USERNAME']; ?></td>
      <td><?php echo $row['EMAIL']; ?></td>
      <td><?php echo $row['PHONE_NUMBER']; ?></td>
      <td><?php echo $row['ESPB']; ?></td>
      <td><?php echo $row['BALANCE']; ?></td>
      <td><?php echo $row['STUDENT_INDEX']; ?></td>
      <td><?php echo $row['JMBG']; ?></td>
      <td><?php echo $row['STUDY_STATUS']; ?></td>
      <td>
        <a href="edit.php?edit=<?php echo $row['ID']; ?>" class="edit_btn" >Edit</a>
      </td>
      <td>
        <a href="admin.php?del=<?php echo $row['ID']; ?>" class="del_btn">Delete</a>
      </td>
    </tr>
  <?php } ?>
</table></div>


<!--FOR VIEWING PROFESSORS-->

    <div class="content6">
     <?php $results = mysqli_query($con, "SELECT * FROM fr_users where USER_TYPE=2 "); ?>

<table>
  <thead>
    <tr>
      <th>First name</th>
      <th>Last name</th>
      <th>Username</th>
      <th>Email</th>
      <th>Phone</th>
      <th>Parent</th>
      <th>JMBG</th>

      <th colspan="2">Action</th>
    </tr>
  </thead>
  
  <?php while ($row = mysqli_fetch_array($results)) { ?>
    <tr>
      <td><?php echo $row['FIRSTNAME']; ?></td>
      <td><?php echo $row['LASTNAME']; ?></td>
      <td><?php echo $row['USERNAME']; ?></td>
      <td><?php echo $row['EMAIL']; ?></td>
      <td><?php echo $row['PHONE_NUMBER']; ?></td>
      <td><?php echo $row['PARENT_NAME']; ?></td>
      <td><?php echo $row['JMBG']; ?></td>
      
      <td>
        <a href="edit.php?edit=<?php echo $row['ID']; ?>" class="edit_btn" >Edit</a>
      </td>
      <td>
        <a href="admin.php?del=<?php echo $row['ID']; ?>" class="del_btn">Delete</a>
      </td>
    </tr>
  <?php } ?>
</table></div>


<!--FOR VIEWING ADMINISTRATIVE EMPLOYEE-->

    <div class="content7">
     <?php $results = mysqli_query($con, "SELECT * FROM fr_users where USER_TYPE=4 "); ?>

<table>
  <thead>
    <tr>
      <th>First name</th>
      <th>Last name</th>
      <th>Username</th>
      <th>Email</th>
      <th>Phone</th>
      <th>Parent</th>
      <th>JMBG</th>
     
      <th colspan="2">Action</th>
    </tr>
  </thead>
  
  <?php while ($row = mysqli_fetch_array($results)) { ?>
    <tr>
      <td><?php echo $row['FIRSTNAME']; ?></td>
      <td><?php echo $row['LASTNAME']; ?></td>
      <td><?php echo $row['USERNAME']; ?></td>
      <td><?php echo $row['EMAIL']; ?></td>
      <td><?php echo $row['PHONE_NUMBER']; ?></td>
      <td><?php echo $row['PARENT_NAME']; ?></td>
      <td><?php echo $row['JMBG']; ?></td>
      
      <td>
        <a href="edit.php?edit=<?php echo $row['ID']; ?>" class="edit_btn" >Edit</a>
      </td>
      <td>
        <a href="admin.php?del=<?php echo $row['ID']; ?>" class="del_btn">Delete</a>
      </td>
    </tr>
  <?php } ?>
</table></div>

<!--FOR VIEWING EXAMS-->

    <div class="content8">
     <?php $results = mysqli_query($con, "SELECT * FROM fr_exams  "); ?>
 
<table>
  <thead>
    <tr>
    <th>Id</th>
      <th>Name</th>
      <th>ESPB</th>
      <th>Mandatory</th>
     
      <th colspan="2">Action</th>
    </tr>
  </thead>
  
  <?php while ($row = mysqli_fetch_array($results)) { ?>
    <tr>
    <td><?php echo $row['EXAM_ID']; ?></td>
      <td><?php echo $row['EXAM_NAME']; ?></td>
      <td><?php echo $row['ESPB']; ?></td>
      <td><?php if($row['MANDATORY']=="1")echo "Yes";
                  else echo "No"; ?></td>
      
      <td>
        <a href="editExam.php?edit=<?php echo $row['EXAM_ID']; ?>" class="edit_btn" >Edit</a>
      </td>
      <td>
        <a href="admin.php?del_course=<?php echo $row['EXAM_ID']; ?>" class="del_btn">Delete</a>
      </td>
    </tr>
  <?php } ?>
</table>
</div>
<!--FOR VIEWING COURSES-->

    <div class="content10">
     <?php $results = mysqli_query($con, "SELECT * FROM fr_courses  "); ?>
 
<table>
  <thead>
    <tr>
    <th>Id</th>
      <th>Name</th>
      <th>ESPB</th>
      
     
      <th colspan="2">Action</th>
    </tr>
  </thead>
  
  <?php while ($row = mysqli_fetch_array($results)) { ?>
    <tr>
    <td><?php echo $row['COURSE_ID']; ?></td>
      <td><?php echo $row['COURSE_NAME']; ?></td>
      <td><?php echo $row['COURSE_ESPB']; ?></td>
      
      
      <td>
        <a href="editCourse.php?edit=<?php echo $row['COURSE_ID']; ?>" class="edit_btn" >Edit</a>
      </td>
      <td>
        <a href="admin.php?del_exam=<?php echo $row['COURSE_ID']; ?>" class="del_btn">Delete</a>
      </td>
    </tr>
  <?php } ?>
</table>
</div>
<!--FOR VIEWING EXAMS-->

    <div class="content12">
     <?php $results = mysqli_query($con, "SELECT * FROM fr_courses  "); ?>
 
<table>
  <thead>
    <tr>
      <th>Course id</th>
      <th>Exam id</th>
      <th>Course name</th>
      <th>Exam name</th>
      <th>Semester</th>
     
      <th colspan="2">Action</th>
    </tr>
  </thead>
  
  <?php while ($row = mysqli_fetch_array($results)) { 
                $crs_id=$row['COURSE_ID'];
                $results2 = mysqli_query($con, "SELECT * FROM fr_exams_courses WHERE COURSE_ID = $crs_id");
                while ($row2 = mysqli_fetch_array($results2)) {
                  $exm_id=$row2['EXAM_ID'];
                  $results3 = mysqli_query($con, "SELECT * FROM fr_exams WHERE EXAM_ID = $exm_id");
                  $row3 = mysqli_fetch_array($results3)?>
    <tr>
      <td><?php echo $row2['COURSE_ID']; ?></td>
      <td><?php echo $row2['EXAM_ID']; ?></td>
      <td><?php echo $row['COURSE_NAME']; ?></td>
      <td><?php echo $row3['EXAM_NAME']; ?></td>
      <td><?php echo $row2['SEMESTER'];?></td>
      
      
      <td>
        <a href="admin.php?del_exam_courses=<?php echo $row2['EXAMS_COURSES_ID']; ?>" class="del_btn">Delete</a>
      </td>
    </tr>
  <?php }} ?>
</table>
</div>

<script>

      $('.serv-btn1').click(function(){
        $('nav ul .serv-show1').toggleClass("show1");
        $('nav ul .first').toggleClass("rotate");
        $('nav ul .serv-show2').removeClass("show1");
        $('nav ul .serv-show3').removeClass("show1");
        $('nav ul .serv-show4').removeClass("show1");
        $('nav ul .serv-show5').removeClass("show1");
        $('nav ul .second').removeClass("rotate");
        $('nav ul .third').removeClass("rotate");
        $('nav ul .fourth').removeClass("rotate");
        $('nav ul .fifth').removeClass("rotate");
       
      });
      $('.serv-btn2').click(function(){
        $('nav ul .serv-show2').toggleClass("show1");
        $('nav ul .second').toggleClass("rotate");
        $('nav ul .serv-show1').removeClass("show1");
        $('nav ul .serv-show3').removeClass("show1");
        $('nav ul .serv-show4').removeClass("show1");
        $('nav ul .serv-show5').removeClass("show1");
        $('nav ul .first').removeClass("rotate");
        $('nav ul .third').removeClass("rotate");
        $('nav ul .fourth').removeClass("rotate");
        $('nav ul .fifth').removeClass("rotate");
      });
      $('.serv-btn3').click(function(){
        $('nav ul .serv-show3').toggleClass("show1");
        $('nav ul .third').toggleClass("rotate");
        $('nav ul .serv-show1').removeClass("show1");
        $('nav ul .serv-show2').removeClass("show1");
        $('nav ul .serv-show4').removeClass("show1");
        $('nav ul .serv-show5').removeClass("show1");
        $('nav ul .second').removeClass("rotate");
        $('nav ul .first').removeClass("rotate");
        $('nav ul .fourth').removeClass("rotate");
        $('nav ul .fifth').removeClass("rotate");
        
      });
      $('.serv-btn4').click(function(){
        $('nav ul .serv-show4').toggleClass("show1");
        $('nav ul .fourth').toggleClass("rotate");
        $('nav ul .serv-show1').removeClass("show1");
        $('nav ul .serv-show2').removeClass("show1");
        $('nav ul .serv-show3').removeClass("show1");
        $('nav ul .serv-show5').removeClass("show1");
        $('nav ul .second').removeClass("rotate");
        $('nav ul .third').removeClass("rotate");
        $('nav ul .first').removeClass("rotate");
        $('nav ul .fifth').removeClass("rotate");
      });
      $('.serv-btn5').click(function(){
        $('nav ul .serv-show5').toggleClass("show1");
        $('nav ul .fifth').toggleClass("rotate");
        $('nav ul .serv-show1').removeClass("show1");
        $('nav ul .serv-show2').removeClass("show1");
        $('nav ul .serv-show3').removeClass("show1");
        $('nav ul .serv-show4').removeClass("show1");
        $('nav ul .second').removeClass("rotate");
        $('nav ul .third').removeClass("rotate");
        $('nav ul .first').removeClass("rotate");
        $('nav ul .fourth').removeClass("rotate");
      });
      $('nav ul li').click(function(){
        $(this).addClass("active").siblings().removeClass("active");
      });


      $('.addStudent').click(function(){    
        $('.content1').toggleClass("show1");
        $('.content2').removeClass("show2");
        $('.content3').removeClass("show2");
        $('.content4').removeClass("show2");
        $('.content5').removeClass("show2");
        $('.content6').removeClass("show2");
        $('.content7').removeClass("show2");
        $('.content8').removeClass("show2");
        $('.content9').removeClass("show2");
        $('.content10').removeClass("show2");
        $('.content11').removeClass("show2");
        $('.content12').removeClass("show2");
      });
      $('.addCourse').click(function(){    
        $('.content2').toggleClass("show2");
        $('.content1').removeClass("show1");
        $('.content3').removeClass("show2");
        $('.content4').removeClass("show2");
        $('.content5').removeClass("show2");
        $('.content6').removeClass("show2");
        $('.content7').removeClass("show2");
        $('.content8').removeClass("show2");
        $('.content9').removeClass("show2");
        $('.content10').removeClass("show2");
        $('.content11').removeClass("show2");
        $('.content12').removeClass("show2");
      });
      $('.addProfessor').click(function(){    
        $('.content3').toggleClass("show2");
        $('.content1').removeClass("show1");
        $('.content2').removeClass("show2");
        $('.content4').removeClass("show2");
        $('.content5').removeClass("show2");
        $('.content6').removeClass("show2");
        $('.content7').removeClass("show2");
        $('.content8').removeClass("show2");
        $('.content9').removeClass("show2");
        $('.content10').removeClass("show2");
        $('.content11').removeClass("show2");
        $('.content12').removeClass("show2");
      });

      $('.addEmployee').click(function(){    
        $('.content4').toggleClass("show2");
        $('.content1').removeClass("show1");
        $('.content3').removeClass("show2");
        $('.content2').removeClass("show2");
        $('.content5').removeClass("show2");
        $('.content6').removeClass("show2");
        $('.content7').removeClass("show2");
        $('.content8').removeClass("show2");
        $('.content9').removeClass("show2");
        $('.content10').removeClass("show2");
        $('.content11').removeClass("show2");
        $('.content12').removeClass("show2");
      });
       $('.addExam').click(function(){    
        $('.content9').toggleClass("show2");
        $('.content1').removeClass("show1");
        $('.content3').removeClass("show2");
        $('.content2').removeClass("show2");
        $('.content5').removeClass("show2");
        $('.content6').removeClass("show2");
        $('.content7').removeClass("show2");
        $('.content8').removeClass("show2");
        $('.content4').removeClass("show2");
        $('.content10').removeClass("show2");
        $('.content11').removeClass("show2");
        $('.content12').removeClass("show2");
      });

      $('.viewStudent').click(function(){    
        $('.content5').toggleClass("show2");
        $('.content1').removeClass("show1");
        $('.content3').removeClass("show2");
        $('.content4').removeClass("show2");
        $('.content2').removeClass("show2");
        $('.content6').removeClass("show2");
        $('.content7').removeClass("show2");
        $('.content8').removeClass("show2");
        $('.content9').removeClass("show2");
        $('.content10').removeClass("show2");
        $('.content11').removeClass("show2");
        $('.content12').removeClass("show2");
      });
      $('.viewProfessor').click(function(){    
        $('.content6').toggleClass("show2");
        $('.content1').removeClass("show1");
        $('.content3').removeClass("show2");
        $('.content4').removeClass("show2");
        $('.content5').removeClass("show2");
        $('.content2').removeClass("show2");
        $('.content7').removeClass("show2");
        $('.content8').removeClass("show2");
        $('.content9').removeClass("show2");
        $('.content10').removeClass("show2");
        $('.content11').removeClass("show2");
        $('.content12').removeClass("show2");
      });
      $('.viewEmployee').click(function(){    
        $('.content7').toggleClass("show2");
        $('.content1').removeClass("show1");
        $('.content3').removeClass("show2");
        $('.content4').removeClass("show2");
        $('.content5').removeClass("show2");
        $('.content6').removeClass("show2");
        $('.content2').removeClass("show2");
        $('.content8').removeClass("show2");
        $('.content9').removeClass("show2");
        $('.content10').removeClass("show2");
        $('.content11').removeClass("show2");
        $('.content12').removeClass("show2");
      });
      $('.viewCourse').click(function(){    
        $('.content8').toggleClass("show2");
        $('.content1').removeClass("show1");
        $('.content3').removeClass("show2");
        $('.content4').removeClass("show2");
        $('.content5').removeClass("show2");
        $('.content6').removeClass("show2");
        $('.content7').removeClass("show2");
        $('.content2').removeClass("show2");
        $('.content9').removeClass("show2");
        $('.content10').removeClass("show2");
        $('.content11').removeClass("show2");
        $('.content12').removeClass("show2");
      });
      $('.viewExam').click(function(){   
        $('.content10').toggleClass("show2");
        $('.content1').removeClass("show1");
        $('.content3').removeClass("show2");
        $('.content4').removeClass("show2");
        $('.content5').removeClass("show2");
        $('.content6').removeClass("show2");
        $('.content7').removeClass("show2");
        $('.content2').removeClass("show2");
        $('.content8').removeClass("show2");
        $('.content9').removeClass("show2");
        $('.content11').removeClass("show2");
        $('.content12').removeClass("show2");
      });
      $('.connectExams').click(function(){   
        $('.content11').toggleClass("show2");
        $('.content1').removeClass("show1");
        $('.content3').removeClass("show2");
        $('.content4').removeClass("show2");
        $('.content5').removeClass("show2");
        $('.content6').removeClass("show2");
        $('.content7').removeClass("show2");
        $('.content2').removeClass("show2");
        $('.content8').removeClass("show2");
        $('.content9').removeClass("show2");
        $('.content10').removeClass("show2");
        $('.content12').removeClass("show2");
      });
      $('.viewConnectExams').click(function(){   
        $('.content12').toggleClass("show2");
        $('.content1').removeClass("show1");
        $('.content3').removeClass("show2");
        $('.content4').removeClass("show2");
        $('.content5').removeClass("show2");
        $('.content6').removeClass("show2");
        $('.content7').removeClass("show2");
        $('.content2').removeClass("show2");
        $('.content8').removeClass("show2");
        $('.content9').removeClass("show2");
        $('.content10').removeClass("show2");
        $('.content11').removeClass("show2");
      });

    </script>
  </body>
</html>
