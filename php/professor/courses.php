<?php
    session_start();
    
    // check if the user's logged in.
    if(!isset($_SESSION['userType'])){
        header("Location: index.php");
        die();
    }
    
    // let only professors access professor pages.
    if($_SESSION['userType'] != 2)
    {
        die();
    }
    
    $DATABASE_HOST = 'localhost';
    $DATABASE_USER = 'root';
    $DATABASE_PASS = '';
    $DATABASE_NAME = 'freshman';

    $exams = array();
    global $failedNumber, $totalNumber, $passedNumber, $passRate, $totalSestica, $totalSedmica, $totalOsmica, $totalDevetki, $totalDesetki, $averageNumber;

    $connection = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);    
    
    if ( !$connection )
    {
        echo 'Unable to connect with database ';
    }
    else 
    {
        if(isset($_POST['show_statistics'])){
            if(!empty($_POST['studyYear'])){
                if (!empty($_POST['nameOfExam'])){
                    $selectedYear = $_POST['studyYear'];
                    $selectedExam = $_POST['nameOfExam'];

                    #Number of failed students
                    $queryFailed = "SELECT COUNT(*) FROM FR_USER_EXAMS WHERE PASSED = 0 AND EXAM_ID = " . $selectedExam . " AND PASSING_YEAR = " . $selectedYear . ";";
                    $resultFailed = mysqli_query($connection, $queryFailed);
                    while($exam_info = mysqli_fetch_row($resultFailed))
                    {
                        $failedNumber = ucfirst($exam_info[0]);
                    }

                    #Total students
                    $queryTotal = "SELECT COUNT(*) FROM FR_USER_EXAMS WHERE EXAM_ID = " . $selectedExam . " AND PASSING_YEAR = " . $selectedYear . ";";
                    $resultTotal = mysqli_query($connection, $queryTotal);
                    while($exam_info = mysqli_fetch_row($resultTotal))
                    {
                        $totalNumber = ucfirst($exam_info[0]);
                    }

                    #Passed the exam
                    $queryPassed = "SELECT COUNT(*) FROM FR_USER_EXAMS WHERE PASSED = 1 AND EXAM_ID = " . $selectedExam . " AND PASSING_YEAR = " . $selectedYear . ";";
                    $resultPassed = mysqli_query($connection, $queryPassed);
                    while($exam_info = mysqli_fetch_row($resultPassed))
                    {
                        $passedNumber = ucfirst($exam_info[0]);
                    }

                    #Srednja vrednost
                    $queryAvg = "SELECT CAST(AVG(GRADE) AS DECIMAL(10,2)) FROM FR_USER_EXAMS WHERE EXAM_ID = " . $selectedExam . " AND PASSING_YEAR = " . $selectedYear . ";";
                    $resultAvg = mysqli_query($connection, $queryAvg);
                    while($exam_info = mysqli_fetch_row($resultAvg))
                    {
                        $averageNumber = ucfirst($exam_info[0]);
                    }

                    #Broj sestica
                    $querySestica = "SELECT COUNT(*) FROM FR_USER_EXAMS WHERE GRADE = 6 AND EXAM_ID = " . $selectedExam . " AND PASSING_YEAR = " . $selectedYear . ";";
                    $resultSestica = mysqli_query($connection, $querySestica);
                    while($exam_info = mysqli_fetch_row($resultSestica))
                    {
                        $totalSestica = ucfirst($exam_info[0]);
                    }

                    #Broj sedmica
                    $querySedmica = "SELECT COUNT(*) FROM FR_USER_EXAMS WHERE GRADE = 7 AND EXAM_ID = " . $selectedExam . " AND PASSING_YEAR = " . $selectedYear . ";";
                    $resultSedmica = mysqli_query($connection, $querySedmica);
                    while($exam_info = mysqli_fetch_row($resultSedmica))
                    {
                        $totalSedmica = ucfirst($exam_info[0]);
                    }

                    #Broj osmica
                    $queryOsmica = "SELECT COUNT(*) FROM FR_USER_EXAMS WHERE GRADE = 8 AND EXAM_ID = " . $selectedExam . " AND PASSING_YEAR = " . $selectedYear . ";";
                    $resultOsmica = mysqli_query($connection, $queryOsmica);
                    while($exam_info = mysqli_fetch_row($resultOsmica))
                    {
                        $totalOsmica = ucfirst($exam_info[0]);
                    }

                    #Broj devetki
                    $queryDevetki = "SELECT COUNT(*) FROM FR_USER_EXAMS WHERE GRADE = 9 AND EXAM_ID = " . $selectedExam . " AND PASSING_YEAR = " . $selectedYear . ";";
                    $resultDevetki = mysqli_query($connection, $queryDevetki);
                    while($exam_info = mysqli_fetch_row($resultDevetki))
                    {
                        $totalDevetki = ucfirst($exam_info[0]);
                    }

                    #Broj desetki
                    $queryDesetki = "SELECT COUNT(*) FROM FR_USER_EXAMS WHERE GRADE = 10 AND EXAM_ID = " . $selectedExam . " AND PASSING_YEAR = " . $selectedYear . ";";
                    $resultDesetki = mysqli_query($connection, $queryDesetki);
                    while($exam_info = mysqli_fetch_row($resultDesetki))
                    {
                        $totalDesetki = ucfirst($exam_info[0]);
                    }

                    if ($totalNumber != 0) {
                        $passRate = ($passedNumber * 100) / $totalNumber . "%";
                    } else {
                        $passRate = 0 . "%";
                    } 
                }
            }
        }
    }
?>



<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

        <title>Professor exams</title>
    </head>
    <body class="bg-light">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <img src="../../images/logo.svg" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="../professor.php">Home Page<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="grades.php">Grades</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Exams</a>
                    </li>
                </ul>
                <div class="navbar-nav nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php if(isset($_SESSION['username'])){echo ucfirst($_SESSION['username']);}; ?>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="../logout.php">Logout</a>
                    </div>
                </div>
            </div>
        </nav>

        <br>
        <div class="container">
            <?php
                $queryPeriod = "SELECT DISTINCT PASSING_YEAR FROM FR_USER_EXAMS WHERE PASSING_YEAR IS NOT NULL;";
                $queryExam = "SELECT EXAM_NAME, EXAM_ID FROM FR_EXAMS WHERE PROFESSOR_ID = (SELECT ID FROM FR_USERS WHERE USERNAME = '" . $_SESSION['username'] . "');";
                $resultPeriod = mysqli_query($connection, $queryPeriod);
                $resultExam = mysqli_query($connection, $queryExam);
    
                echo "<form action='' method='post'> <label style='padding-right: 10px;'> Choose year: </label> <select name = 'studyYear'    style='margin-bottom: 20px;'> <option value='' disabled selected>Choose year</option>";
                while ($row = mysqli_fetch_array($resultPeriod)) {
                    echo "<option value = '" . $row['PASSING_YEAR'] . "'>" . $row['PASSING_YEAR'] . "</option>";
                }
                echo "</select> <label style='padding-right: 10px; padding-left: 40px;'> Choose exam: </label> <select name = 'nameOfExam' style='margin-bottom: 20px;'> <option value='' disabled selected>Choose exam</option>";
                while ($rowExam = mysqli_fetch_array($resultExam)) {
                    echo "<option value = '" . $rowExam['EXAM_ID'] . "'>" . $rowExam['EXAM_NAME'] . "</option> ";
                }
                echo "</select> <input type='submit' style='margin-left: 25px;background-color: #343a40;color: #fff;border-block-style: hidden;' name='show_statistics' value='Show'> </form>";
            ?>
            <div class="row">
                <form>
                <h4 style="margin-top: 20px;">Elementary</h4>
                <table class="table table-light" style="text-align: center;">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Total students</th>
                            <th scope="col">Passed the exam</th>
                            <th scope="col">Failed</th>
                            <th scope="col">Pass rate</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            echo "<tr>";  
                            echo "<td>" . $totalNumber . "</td>";
                            echo "<td>" . $passedNumber . "</td>";
                            echo "<td>" . $failedNumber . "</td>";
                            echo "<td>" . $passRate . "</td>";
                            echo "</tr>";      
                        ?>
                    </tbody>
                </table>                
                <h4 style="margin-top: 50px;">Grades</h4>
                <table class="table table-light" style="text-align: center;">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col"> </th>
                            <th scope="col">6</th>
                            <th scope="col">7</th>
                            <th scope="col">8</th>
                            <th scope="col">9</th>
                            <th scope="col">10</th>
                            <th scope="col">Average</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            echo "<tr>";  
                            echo "<td> Total </td>";
                            echo "<td>" . $totalSestica . "</td>";
                            echo "<td>" . $totalSedmica . "</td>";
                            echo "<td>" . $totalOsmica . "</td>";
                            echo "<td>" . $totalDevetki . "</td>";
                            echo "<td>" . $totalDesetki . "</td>";
                            echo "<td>" . $averageNumber . "</td>";
                            echo "</tr>";      
                        ?>
                    </tbody>
                </table>
                </form>
            </div>
        </div>


        <footer class="page-footer fixed-bottom font-small bg-dark ">   
            <div class="container">
                <div class="text-light footer-copyright text-center py-3">© 2020 Copyright:
                    <a href="https://facebook.com/"> Freshman.com</a>
                </div>
            </div>
        </footer>

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    </body>
</html>