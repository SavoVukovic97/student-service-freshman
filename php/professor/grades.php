<?php
    include "./models/grade.php";
    session_start();
    
    // check if the user's logged in.
    if(!isset($_SESSION['userType'])){
        header("Location: index.php");
        die();
    }
    
    // let only professors access professor pages.
    if($_SESSION['userType'] != 2)
    {
        die();
    }
    
    $DATABASE_HOST = 'localhost';
    $DATABASE_USER = 'root';
    $DATABASE_PASS = '';
    $DATABASE_NAME = 'freshman';
    
    global $selectedPeriod;

    $connection = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);    
    
    $grades = array();

    if ( !$connection )
    {
        echo 'Unable to connect with database ';
    } 
    else
    {
        if(isset($_POST['show_students'])){
            if(!empty($_POST['periodOfExam'])){
                if (!empty($_POST['nameOfExam'])){
                    $selectedPeriod = $_POST['periodOfExam'];
                    $selectedExam = $_POST['nameOfExam'];
                    $query = "SELECT FR_USERS.ID, FR_USERS.FIRSTNAME, FR_USERS.LASTNAME, FR_USERS.STUDENT_INDEX, FR_EXAMS.EXAM_ID, FR_EXAMS.EXAM_NAME, FR_EXAMS.ESPB FROM FR_USERS INNER JOIN FR_USER_EXAM_REGISTRATION ON FR_USERS.ID = FR_USER_EXAM_REGISTRATION.ID INNER JOIN FR_EXAMS ON FR_USER_EXAM_REGISTRATION.EXAM_ID = FR_EXAMS.EXAM_ID WHERE FR_EXAMS.PROFESSOR_ID = (SELECT ID FROM FR_USERS WHERE USERNAME = '" . $_SESSION['username'] . "') AND FR_USER_EXAM_REGISTRATION.EXAM_ID = " . $selectedExam . " AND FR_USER_EXAM_REGISTRATION.EXAM_PERIOD_ID = " . $selectedPeriod . ";";
                    $result = mysqli_query($connection, $query);
                    
                    while($grade_info = mysqli_fetch_row($result))
                    {
                        $grade = new Grade;
                        $grade->studentID = ucfirst($grade_info[0]);
                        $grade->studentName = ucfirst($grade_info[1]);
                        $grade->studentLastName = ucfirst($grade_info[2]);
                        $grade->studentStudentIndex = ucfirst($grade_info[3]);
                        $grade->examID = ucfirst($grade_info[4]);
                        $grade->examName = ucfirst($grade_info[5]);
                        $grade->examESPB = ucfirst($grade_info[6]);
                        
                        $grades[] = $grade;
                    }
                }
            }
        }
    }
?>


<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

        <title>Entering grades</title>
    </head>
    <body class="bg-light">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <img src="../../images/logo.svg" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="../professor.php">Home Page<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Grades</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="courses.php">Exams</a>
                    </li>
                </ul>
                <div class="navbar-nav nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php if(isset($_SESSION['username'])){echo ucfirst($_SESSION['username']);} ?>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="../logout.php">Logout</a>
                    </div>
                </div>
            </div>
        </nav>

        <br>    
        <div class="container">
            <?php
                $queryPeriod = "SELECT EXAM_PERIOD_NAME, EXAM_PERIOD_ID FROM FR_EXAM_PERIOD;";
                $queryExam = "SELECT EXAM_NAME, EXAM_ID FROM FR_EXAMS WHERE PROFESSOR_ID = (SELECT ID FROM FR_USERS WHERE USERNAME = '" . $_SESSION['username'] . "');";
                $resultPeriod = mysqli_query($connection, $queryPeriod);
                $resultExam = mysqli_query($connection, $queryExam);
    
                echo "<form action='' method='post'> <label style='padding-right: 10px;'> Choose exam period: </label> <select name = 'periodOfExam'    style='margin-bottom: 20px;'> <option value='' disabled selected>Choose exam period</option>";
                while ($row = mysqli_fetch_array($resultPeriod)) {
                    echo "<option value = '" . $row['EXAM_PERIOD_ID'] . "'>" . $row['EXAM_PERIOD_NAME'] . "</option>";
                }
                echo "</select> <label style='padding-right: 10px; padding-left: 40px;'> Choose exam: </label> <select name = 'nameOfExam' style='margin-bottom: 20px;'> <option value='' disabled selected>Choose exam</option>";
                while ($rowExam = mysqli_fetch_array($resultExam)) {
                    echo "<option value = '" . $rowExam['EXAM_ID'] . "'>" . $rowExam['EXAM_NAME'] . "</option> ";
                }
                echo "</select> <input type='submit' style='margin-left: 25px;background-color: #343a40;color: #fff;border-block-style: hidden;' name='show_students' value='Show'> </form>";
            ?>

            <form action='' method='post'>
            <div class="row" style="margin-top: 0px;">
                <table class="table table-light" id="student_grades" textstyle="text-align: center;">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Last name</th>
                            <th scope="col">Index</th>
                            <th scope="col">Exam name</th>
                            <th scope="col">ESPB</th>
                            <th scope="col">Grade</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $count = 1;
                            foreach($grades as $grade){
                                echo "<tr>";
                                echo "<th scope='row'>" . $count++ . "</td>";    
                                echo "<td>" . $grade->studentName . "</td>";
                                echo "<td>" . $grade->studentLastName . "</td>";
                                echo "<td>" . $grade->studentStudentIndex . "</td>";    
                                echo "<td>" . $grade->examName . "</td>";    
                                echo "<td>" . $grade->examESPB . "</td>";
                                echo "<td><select name=$count>
                                              <option value=5>5</option>
                                              <option value=6>6</option>
                                              <option value=7>7</option>
                                              <option value=8>8</option>
                                              <option value=9>9</option>
                                              <option value=10>10</option>
                                            </select></td>";
                                echo "</tr>";
                            }
                            echo '<input type="hidden" name="gradesArray" value="'.htmlspecialchars(json_encode($grades)).'">';
                            echo "<input type='hidden' name='selectedPeriod' value='" . $selectedPeriod . "'>";
                        ?>
                    </tbody>
                </table>
                <input type="submit" name="add_grades" style="background-color: #343a40;color: #fff;border-block-style: hidden;" value="Enter grades">
            </form>
                
                <?php    
                if(isset($_POST['add_grades'])){
                    $grades = json_decode(htmlspecialchars_decode($_POST['gradesArray']));
                    if (count($grades) > 0) {
                        $gradesFromTable = array();
                        $grades = json_decode(htmlspecialchars_decode($_POST['gradesArray']));
                        $count = count($grades);
                        $selectedPeriod = $_POST['selectedPeriod'];
                        $year = date("Y");

                        for ($i=2; $i < $count + 2; $i++) { 
                            array_push($gradesFromTable, $_POST['' .  $i]);
                        }
                        #echo $grades[0]->studentID;

                        for ($i=0; $i < count($gradesFromTable); $i++) { 
                            
                            $ocena = $gradesFromTable[$i];
                            $student = $grades[$i]->studentID;
                            $indeks = $grades[$i]->studentStudentIndex;
                            $predmet = $grades[$i]->examID;

                            $queryGrade = "UPDATE FR_USER_EXAMS SET GRADE = " . $ocena . ", PROFESSOR = (SELECT CONCAT(FIRSTNAME, ' ', LASTNAME) FROM FR_USERS WHERE USERNAME = '" . $_SESSION['username'] . "'), EXAM_PERIOD_ID = " . $selectedPeriod . ", PASSING_YEAR = " . $year . " WHERE ID = " . $student . " AND EXAM_ID = " . $predmet . ";";
                            if(!mysqli_query($connection, $queryGrade)){
                                $message = "Grades was not added for student with index" . $indeks;
                                echo "<script type='text/javascript'>alert('$message');</script>";
                            }
                        }
                        $message = "Grades added successfully";
                        echo "<script type='text/javascript'>alert('$message');</script>";
                    }
                    else{
                        $message = "There is no entered grades";
                        echo "<script type='text/javascript'>alert('$message');</script>";
                    }
                }
                ?>
            </div>
        </div>



        <footer class="page-footer fixed-bottom font-small bg-dark ">   
            <div class="container">
                <div class="text-light footer-copyright text-center py-3">© 2020 Copyright:
                    <a href="https://facebook.com/"> Freshman.com</a>
                </div>
            </div>
        </footer>

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    </body>
</html>