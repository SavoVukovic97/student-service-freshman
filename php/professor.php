<?php
    session_start();
    
    // check if the user's logged in.
    if(!isset($_SESSION['userType'])){
        header("Location: index.php");
    }
    
    // let only professors access professor pages.
    if($_SESSION['userType'] != 2)
    {
        die();
    }
    
    $DATABASE_HOST = 'localhost';
    $DATABASE_USER = 'root';
    $DATABASE_PASS = '';
    $DATABASE_NAME = 'freshman';
    
    $connection = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);    
    
    if ( !$connection )
    {
        echo 'Unable to connect with database ';
    } 
    else
    {
        $query = "SELECT * FROM fr_users WHERE USERNAME = '" . $_SESSION['username'] . "';";
        $result = mysqli_query($connection, $query);
        while($user_info = mysqli_fetch_row($result))
        {
            $firstName = ucfirst($user_info[2]);
            $lastName = ucfirst($user_info[3]);
            $email = ucfirst($user_info[4]);
            $phoneNumber = ucfirst($user_info[6]);
        }
    }
?>


<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

        <title>Profesor</title>
    </head>
    <body class="bg-light">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <img src="../images/logo.svg" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home Page<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./professor/grades.php">Grades</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./professor/courses.php">Exams</a>
                    </li>
                </ul>
                <div class="navbar-nav nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php if(isset($_SESSION['username'])){echo ucfirst($_SESSION['username']);}; ?>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="logout.php">Logout</a>
                    </div>
                </div>
            </div>
        </nav>

        <div class="container pt-3">
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <div class="jumbotron">
                        <h2 class="display-5 text-center"><?php echo $firstName . " " . $lastName ; ?></h2>          
                        <hr class="my-4">
                        <p>
                        <span class="media text-muted">Email:</span><?php echo $email; ?> 
                        </p>
                        <p> 
                        <span class="media text-muted">Phone Number:</span><?php echo $phoneNumber; ?>
                        </p>
                    </div>          
                </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"> 
        </div>

        <footer class="page-footer fixed-bottom font-small bg-dark ">   
            <div class="container">
                <div class="text-light footer-copyright text-center py-3">© 2020 Copyright:
                    <a href="https://facebook.com/"> Freshman.com</a>
                </div>
            </div>
        </footer>

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    </body>
</html>