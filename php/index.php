<?php
session_start();

$DATABASE_HOST = 'localhost';
$DATABASE_USER = 'root';
$DATABASE_PASS = '';
$DATABASE_NAME = 'freshman';

$has_errors = 0;
$ERROR ="" ;

$con = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);

if ( !$con ) {
 
  echo 'Unable to connect with database ';
}

else{

  if(isset($_POST['submit'])){
    
    $uname=$_POST['username'];
    $password=$_POST['password'];
    
    $sql="select * from fr_users where USERNAME='".$uname."'AND PASSWORD='".$password."' ";
    
    $result=mysqli_query($con,$sql);
    $resrows=mysqli_num_rows( $result);



    if( $resrows > 0){
      $_SESSION['username'] = $uname;
     /* if(!empty($_POST["remember"])) {
        setcookie ("member_username",$_POST["username"],time()+ ( 365 * 24 * 60 * 60));
        setcookie ("member_password",$_POST["password"],time()+ ( 365 * 24 * 60 * 60));

      }
      else{
        setcookie ("member_username","",time()+ ( 365 * 24 * 60 * 60));
        setcookie ("member_password","",time()+ ( 365 * 24 * 60 * 60));


      }*/
        while($row = mysqli_fetch_array($result)){
          $_SESSION['userType'] = $row['USER_TYPE'];
          if ($row['USER_TYPE']==1) {//ADMIN
            header("Location: admin.php");
          }
          else if($row['USER_TYPE']==2) {//professor
            header("Location: professor.php");
          }
           else if($row['USER_TYPE']==3) {//student
            header("Location: student.php");
          }
           else if($row['USER_TYPE']==4) {//adm_employee
            header("Location: ad_employee.php");
          }
        }
    }
    else{
      $ERROR = " Your Username or Password is incorrect ";
      $has_errors = 1;
       
    }
        
}}
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
    <link  rel="stylesheet" href="../css/login.css">
  </head>
  <body>
    <div class="login">
      <div class="logo">
        <img src="../images/logo.svg" style="width: 30%;">
      </div>
      <h1>Login</h1>
      <p id="error" <?php if($has_errors) {echo 'style="border:1.5px solid red;"';} ?>><?php if($has_errors) {echo $ERROR ;} ?></p>
      <form method="post">
        <div class="grid">
        <div class= "label">
          <label for="username">
            <i class="fas fa-user"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="username" value="<?php if(isset($_COOKIE["member_username"])) { echo $_COOKIE["member_username"]; } ?>" placeholder="Username" id="username" required>
         </div> 
         <div class= "label">
          <label for="password">
            <i class="fas fa-lock"></i>
          </label>
        </div>
        <div class="input">
        <input type="password" name="password" placeholder="Password" id="password" value="<?php if(isset($_COOKIE["member_username"])) { echo $_COOKIE["member_password"]; } ?>" required>
      </div>
      </div>
<!--div class="rem">
        <input type="checkbox" name="remember"  id="remember" <?php if(isset($_COOKIE["member_username"])) { ?> checked <?php } ?> >Remember me
</div-->
        <input type="submit" name="submit" value="Login">

        

      </form>
    </div>
  </body>
</html>