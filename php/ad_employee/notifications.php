<?php

session_start();

// check if the user's logged in.
if(!isset($_SESSION['userType'])){
    header("Location: index.php");
    die();
}


// let only admin_employee access student pages.
if($_SESSION['userType'] != 4){
    die();
}

$DATABASE_HOST = 'localhost';
$DATABASE_USER = 'root';
$DATABASE_PASS = '';
$DATABASE_NAME = 'freshman';

$has_errors = 0;
$ERROR ="" ;

$connection = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);


if ( !$connection )
{
    echo 'Unable to connect with database ';
}
else
{

}
?>


<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <title>Ad_employee</title>
</head>
<body class="bg-light">
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <img src="../../images/logo.svg" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="../ad_employee.php">Home Page<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="register_student.php">Student register</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="schedule_exam_period.php">Schedule exam period</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="student_balance_changes.php">Funds balance</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="student_pass_year.php">Enrollment and renewal</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="student_pass_exam.php">Exam report</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Notifications</a>
            </li>

        </ul>
        <div class="navbar-nav nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php if(isset($_SESSION['username'])){echo ucfirst($_SESSION['username']);}; ?>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="../logout.php">Logout</a>
            </div>
        </div>
    </div>
</nav>

<br><br>
<div class="container">
    <?php $results = mysqli_query($connection, "SELECT user.USERNAME,user.STUDENT_INDEX, 'Upload', a.FILE_NAME FROM fr_user_upload_activity a INNER JOIN fr_users user ON a.USER_ID = user.ID"); ?>

    <table class="table table-light" style="text-align: center">
        <thead class="thead-dark">
        <tr>
            <th>Username</th>
            <th>Index</th>
            <th>Activity</th>
            <th>File name</th>
        </tr>
        </thead>

        <?php while ($row = mysqli_fetch_array($results)) {

            ?>

            <tr>
                <td><?php echo $row['USERNAME']; ?></td>
                <td><?php echo $row['STUDENT_INDEX']; ?></td>
                <td><?php echo $row['Upload']; ?></td>
                <td><?php echo $row['FILE_NAME'] ?></td>
            </tr>
        <?php } ?>
    </table></div>
    <br><br><br><br><br>


<footer class="page-footer fixed-bottom font-small bg-dark ">
    <div class="container">
        <div class="text-light footer-copyright text-center py-3">© 2020 Copyright:
            <a href="https://facebook.com/"> Freshman.com</a>
        </div>
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>