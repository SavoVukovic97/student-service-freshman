
<?php
session_start();

// check if the user's logged in.
if(!isset($_SESSION['userType'])){
  header("Location: index.php");
    die();
}


// let only admin_employee access student pages.
if($_SESSION['userType'] != 4){
  die();
}

$DATABASE_HOST = 'localhost';
$DATABASE_USER = 'root';
$DATABASE_PASS = '';
$DATABASE_NAME = 'freshman';

$has_errors = 0;
$ERROR ="" ;


$connection = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);


 if ( !$connection )
    {
        echo 'Unable to connect with database ';
    } 
    else
    {
        if(isset($_POST['submit'])) {

            $index = $_POST['index'];
            $year=$_POST['year'];

            $queryid = "SELECT ID FROM fr_users WHERE STUDENT_INDEX = '$index' ";
            $result2 = mysqli_query($connection, $queryid);

            $resrows2=mysqli_num_rows( $result2);

            if( $resrows2 > 0){
            while($user_info = mysqli_fetch_row($result2)){
                $id=$user_info[0];
            }

            $queryupdateyear = "UPDATE fr_users SET YEAR='$year' WHERE STUDENT_INDEX = '$index'";
            $result1 = mysqli_query($connection,$queryupdateyear);


            if(!empty($_POST['exam1'])){
                $exam1 = $_POST['exam1'];
                $query1 = "INSERT INTO fr_user_exams (EXAM_ID,ID) VALUES(".$exam1.",".$id.") ";

                $testquerry = "SELECT * FROM fr_user_exams WHERE EXAM_ID='$exam1' AND ID='$id' ";
                $result=mysqli_query($connection,$testquerry);
                $resrows=mysqli_num_rows( $result);

                if( $resrows > 0){
                    #die("Already enrolled in exam $exam1");
                    $message = "Student $index is already enrolled in exam $exam1";
                    echo "<script type='text/javascript'>alert('$message');</script>";
                }elseif (!mysqli_query($connection,$query1)){
                    #die("Error inserting new record");
                    $message = "Error inserting new exam with id $exam1";
                    echo "<script type='text/javascript'>alert('$message');</script>";
                }else{
                    $message = "Student $index enrolled in exam $exam1";
                    echo "<script type='text/javascript'>alert('$message');</script>";
                }
            }


            if(!empty($_POST['exam2'])){
                $exam2 = $_POST['exam2'];
                $query2 = "INSERT INTO fr_user_exams (EXAM_ID,ID) VALUES(".$exam2.",".$id.") ";

                $testquerry = "SELECT * FROM fr_user_exams WHERE EXAM_ID='$exam2' AND ID='$id' ";
                $result=mysqli_query($connection,$testquerry);
                $resrows=mysqli_num_rows( $result);

                if( $resrows > 0){
                    #die("Already enrolled in exam $exam2");
                    $message = "Student $index is already enrolled in exam $exam2";
                    echo "<script type='text/javascript'>alert('$message');</script>";
                } elseif(!mysqli_query($connection,$query2)){
                    #die("Error inserting new record");
                    $message = "Error inserting new exam with id $exam2";
                    echo "<script type='text/javascript'>alert('$message');</script>";
                }else{
                    $message = "Student $index enrolled in exam $exam2";
                    echo "<script type='text/javascript'>alert('$message');</script>";
                }
            }

            if(!empty($_POST['exam3'])){
                $exam3 = $_POST['exam3'];
                $query3 = "INSERT INTO fr_user_exams (EXAM_ID,ID) VALUES(".$exam3.",".$id.") ";

                $testquerry = "SELECT * FROM fr_user_exams WHERE EXAM_ID='$exam3' AND ID='$id' ";
                $result=mysqli_query($connection,$testquerry);
                $resrows=mysqli_num_rows( $result);

                if( $resrows > 0){
                    #die("Already enrolled in exam $exam3");
                    $message = "Student $index is already enrolled in exam $exam3";
                    echo "<script type='text/javascript'>alert('$message');</script>";
                } elseif(!mysqli_query($connection,$query3)){
                    #die("Error inserting new record");
                    $message = "Error inserting new exam with id $exam3";
                    echo "<script type='text/javascript'>alert('$message');</script>";
                }else{
                    $message = "Student $index enrolled in exam $exam3";
                    echo "<script type='text/javascript'>alert('$message');</script>";
                }
            }


            if(!empty($_POST['exam4'])){
                $exam4 = $_POST['exam4'];
                $query4 = "INSERT INTO fr_user_exams (EXAM_ID,ID) VALUES(".$exam4.",".$id.") ";

                $testquerry = "SELECT * FROM fr_user_exams WHERE EXAM_ID='$exam4' AND ID='$id' ";
                $result=mysqli_query($connection,$testquerry);
                $resrows=mysqli_num_rows( $result);

                if( $resrows > 0){
                    #die("Already enrolled in exam $exam4");
                    $message = "Student $index is already enrolled in exam $exam4";
                    echo "<script type='text/javascript'>alert('$message');</script>";
                }elseif(!mysqli_query($connection,$query4)){
                   # die("Error inserting new record");
                    $message = "Error inserting new exam with id $exam4";
                    echo "<script type='text/javascript'>alert('$message');</script>";
                }else{
                    $message = "Student $index enrolled in exam $exam4";
                    echo "<script type='text/javascript'>alert('$message');</script>";
                }
            }


            if(!empty($_POST['exam5'])){
                $exam5 = $_POST['exam5'];
                $query5 = "INSERT INTO fr_user_exams (EXAM_ID,ID) VALUES(".$exam5.",".$id.") ";

                $testquerry = "SELECT * FROM fr_user_exams WHERE EXAM_ID='$exam5' AND ID='$id' ";
                $result=mysqli_query($connection,$testquerry);
                $resrows=mysqli_num_rows( $result);

                if( $resrows > 0){
                    #die("Already enrolled in exam $exam5");
                    $message = "Student $index is already enrolled in exam $exam5";
                    echo "<script type='text/javascript'>alert('$message');</script>";
                } elseif(!mysqli_query($connection,$query5)){
                    #die("Error inserting new record");
                    $message = "Error inserting new exam with id $exam5";
                    echo "<script type='text/javascript'>alert('$message');</script>";
                }else{
                    $message = "Student $index enrolled in exam $exam5";
                    echo "<script type='text/javascript'>alert('$message');</script>";
                }
            }
            }
            else{
                $message = "Student with index $index not found in database";
                echo "<script type='text/javascript'>alert('$message');</script>";
            }
        }
    }
?>


<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

        <title>Ad_employee</title>
    </head>
    <body class="bg-light">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <img src="../../images/logo.svg" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="../ad_employee.php">Home Page<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="register_student.php">Student register</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="schedule_exam_period.php">Schedule exam period</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="student_balance_changes.php">Funds balance</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="student_pass_year.php">Enrollment and renewal</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="student_pass_exam.php">Exam report</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="notifications.php">Notifications</a>
                    </li>

                </ul>
                <div class="navbar-nav nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php if(isset($_SESSION['username'])){echo ucfirst($_SESSION['username']);}; ?>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="../logout.php">Logout</a>
                    </div>
                </div>
            </div>
        </nav>

        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <div class="jumbotron">
                        <h3 class="display-7  text-center">Enter student exams</h3>
                        <hr class="my-4">
                        <form method="post">
                        <label for="index" style="margin-right: 47px">Student index:</label>
                        <input type="text" id="index" name="index" align="right" required><br><br>
                        <label for="year"style="margin-right: 55px">Student year:</label>
                        <input type="text" id="year" name="year" align="right" required><br><br>
                        <br>
                        <label for="exam1" style="margin-right: 75px">Exam no1:</label>
                        <input type="text" id="exam1" name="exam1" align="right" ><br><br>
                        <label for="exam2" style="margin-right: 75px">Exam no2:</label>
                        <input type="text" id="exam2" name="exam2" align="right" ><br><br>
                        <label for="exam3" style="margin-right: 75px">Exam no3:</label>
                        <input type="text" id="exam3" name="exam3" align="right" ><br><br>
                        <label for="exam4" style="margin-right: 75px">Exam no4:</label>
                        <input type="text" id="exam4" name="exam4" align="right" ><br><br>
                        <label for="exam5" style="margin-right: 75px">Exam no5:</label>
                        <input type="text" id="exam5" name="exam5" align="right" ><br><br>

                            <input type="submit" value="Submit" name="submit">
                        </form>
                    </div>
                </div>
            </div>
        </div>



        <footer class="page-footer fixed-bottom font-small bg-dark ">   
            <div class="container">
                <div class="text-light footer-copyright text-center py-3">© 2020 Copyright:
                    <a href="https://facebook.com/"> Freshman.com</a>
                </div>
            </div>
        </footer>

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    </body>
</html>