<?php

session_start();

// check if the user's logged in.
if(!isset($_SESSION['userType'])){
    header("Location: index.php");
    die();
}


// let only admin_employee access student pages.
if($_SESSION['userType'] != 4){
    die();
}

$DATABASE_HOST = 'localhost';
$DATABASE_USER = 'root';
$DATABASE_PASS = '';
$DATABASE_NAME = 'freshman';

$has_errors = 0;
$ERROR ="" ;

$connection = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);


if ( !$connection )
{
    echo 'Unable to connect with database ';
}
else
{
    if(isset($_POST['search'])){

        $index=$_POST['index'];
        #$query1="SELECT ID FROM fr_users WHERE STUDENT_INDEX=$index ";
        $query1="SELECT ID FROM fr_users WHERE STUDENT_INDEX = '" . $index . "';";
        $insert1 =mysqli_query($connection,$query1);

        $resrows=mysqli_num_rows( $insert1);
        if( $resrows > 0){
            $exists=1;
            while($user_info12 = mysqli_fetch_row($insert1)){
                $id=$user_info12[0];
            }
            $results = mysqli_query($connection, "SELECT * FROM fr_user_exams WHERE PASSED=0 AND ID=$id ORDER BY EXAM_ID");
        }else{
            $exists=0;
            $message = "No student with index $index found in database";
            echo "<script type='text/javascript'>alert('$message');</script>";
        }



    }

    if(isset($_POST['renew'])){

            $index=$_POST['index'];
            $balance=$_POST['balance'];
            $status=$_POST['status'];

            #$query1="SELECT ID FROM fr_users WHERE STUDENT_INDEX=$index ";
            $query1="SELECT ID FROM fr_users WHERE STUDENT_INDEX = '" . $index . "';";
            $insert1 =mysqli_query($connection,$query1);

            $resrows=mysqli_num_rows( $insert1);
            if( $resrows > 0){
                while($user_info12 = mysqli_fetch_row($insert1)){
                    $id=$user_info12[0];
                }
                $results = mysqli_query($connection, "SELECT * FROM fr_user_exams WHERE PASSED=0 AND ID=$id ORDER BY EXAM_ID");

                $balancequery = "UPDATE fr_users SET BALANCE = BALANCE - " . $balance . ", STUDY_STATUS = '" . $status . "' WHERE STUDENT_INDEX = '" . $index . "';";
                #$balancequery = "UPDATE fr_users SET BALANCE=$balance,STUDY_STATUS=$status WHERE STUDENT_INDEX= '" . $index . "';";
                if(!mysqli_query($connection,$balancequery)){
                    $message = "Error renewing year";
                    echo "<script type='text/javascript'>alert('$message');</script>";
                }else{
                    $message = "Year renewed";
                    echo "<script type='text/javascript'>alert('$message');</script>";
                }
                #$insert2=mysqli_query($connection,$balancequery);
            }else{
                $message = "No student with index $index found in database";
                echo "<script type='text/javascript'>alert('$message');</script>";
            }

        }

}
?>


<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <title>Ad_employee</title>
</head>
<body class="bg-light">
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <img src="../../images/logo.svg" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="../ad_employee.php">Home Page<span class="sr-only">(current)</span></a>

            </li>
            <li class="nav-item">
                <a class="nav-link" href="register_student.php">Student register</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="schedule_exam_period.php">Schedule exam period</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Funds balance</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="student_pass_year.php">Enrollment and renewal</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="student_pass_exam.php">Exam report</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="notifications.php">Notifications</a>
            </li>

        </ul>
        <div class="navbar-nav nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php if(isset($_SESSION['username'])){echo ucfirst($_SESSION['username']);}; ?>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="../logout.php">Logout</a>
            </div>
        </div>
    </div>
</nav>


<div class="container ">
    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="jumbotron">
                <h3 class="display-7  text-center"> Renew year</h3>
                <hr class="my-4">

                <form method="post" >
                    <label for="index" style="margin-right: 30px">Student index:</label>
                    <input type="text" id="index" name="index" align="right" required><br><br>
                    <input type="submit" value="Search" name="search">
                    <br><br>
                    <label for="balance" style="margin-right: 71px">Balance:</label>
                    <input type="text" id="balance" name="balance" align="right" ><br><br>
                    <label for="status" style="margin-right: 38px">Study Status:</label>
                    <input type="text" id="status" name="status" align="right" ><br><br><br>
                    <input type="submit" value="Renew year" name="renew">
                </form>
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        </div>


        <div class="container">
            <table style="text-align: center" class="table table-light">
                <thead class="thead-dark">
                <tr>
                    <th>Exam id</th>
                    <th>Firstname</th>
                    <th>Lastname</th>
                    <th>Student index</th>
                    <th>Passed</th>
                    <th>Year passed</th>
                    <th>Grade</th>
                    <th>Professor</th>
                    <th>Exam applications</th>

                </tr>
                </thead>
                <?php if(!empty($_POST['index'])){

                    while ($row = mysqli_fetch_array($results)) {

                        $querryidindex = "SELECT STUDENT_INDEX,FIRSTNAME,LASTNAME FROM fr_users WHERE ID=$id";
                        $result1 = mysqli_query($connection, $querryidindex);
                        while($user_info1 = mysqli_fetch_row($result1)){
                            $studentindex=$user_info1[0];
                            $studentname =$user_info1[1];
                            $studentlastname =$user_info1[2];
                         }
                    ?>
                    <tbody>
                    <tr>
                        <td><?php echo $row['EXAM_ID']; ?></td>
                        <td><?php echo $studentname; ?></td>
                        <td><?php echo $studentlastname; ?></td>
                        <td><?php echo $studentindex; ?></td>
                        <td><?php echo $row['PASSED']; ?></td>
                        <td><?php echo $row['PASSING_YEAR'] ?></td>
                        <td><?php echo $row['GRADE']; ?></td>
                        <td><?php echo $row['PROFESSOR']; ?></td>
                        <td><?php echo $row['EXAM_APPLICATIONS']; ?></td>


                    </tr>
                <?php }} ?>
                    </tbody>
            </table></div>
            <br><br><br><br><br><br><br>


        <footer class="page-footer fixed-bottom font-small bg-dark ">
            <div class="container">
                <div class="text-light footer-copyright text-center py-3">© 2020 Copyright:
                    <a href="https://facebook.com/"> Freshman.com</a>
                </div>
            </div>
        </footer>

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>