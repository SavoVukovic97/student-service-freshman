<?php

session_start();

// check if the user's logged in.
if(!isset($_SESSION['userType'])){
  header("Location: index.php");
}


// let only admin_employee access student pages.
if($_SESSION['userType'] != 4){
  die();
}

$DATABASE_HOST = 'localhost';
$DATABASE_USER = 'root';
$DATABASE_PASS = '';
$DATABASE_NAME = 'freshman';

$has_errors = 0;
$ERROR ="" ;

$connection = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);


 if ( !$connection )
    {
        echo 'Unable to connect with database ';
    } 
    else
    {
        if(isset($_POST['searchbalance'])){

            $index=$_POST['index'];
            $balance=$_POST['balance'];

            $testquery = "SELECT * FROM fr_users WHERE STUDENT_INDEX=$index";
            $result=mysqli_query($connection,$testquery);
            $resrows=mysqli_num_rows( $result);

            if( $resrows > 0){
                $sql="UPDATE fr_users SET BALANCE=BALANCE+', '+'$balance' WHERE STUDENT_INDEX='$index'  ";

                if(!mysqli_query($connection,$sql)){
                    #die("Error inserting new record");
                    $message = "Error inserting new record";
                    echo "<script type='text/javascript'>alert('$message');</script>";
                }else{
                    $message = "Funds added successfully";
                    echo "<script type='text/javascript'>alert('$message');</script>";
                    $has_errors = 0;
                }

            }else{
                $message = "Error inserting funds, index not found in database ";
                echo "<script type='text/javascript'>alert('$message');</script>";
            }

           }
    }
?>


<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

        <title>Ad_employee</title>
    </head>
    <body class="bg-light">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <img src="../../images/logo.svg" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="../ad_employee.php">Home Page<span class="sr-only">(current)</span></a>

                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="register_student.php">Student register</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="schedule_exam_period.php">Schedule exam period</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Funds balance</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="student_pass_year.php">Enrollment and renewal</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="student_pass_exam.php">Exam report</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="notifications.php">Notifications</a>
                    </li>

                </ul>
                <div class="navbar-nav nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php if(isset($_SESSION['username'])){echo ucfirst($_SESSION['username']);}; ?>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="../logout.php">Logout</a>
                    </div>
                </div>
            </div>
        </nav>


        <div class="container ">
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <div class="jumbotron">
                        <h3 class="display-7   text-center"> Change balance</h3>
                        <hr class="my-4">
                        <br>
                        <form method="post" >
                            <label for="index">Student index:</label>
                            <input type="text" id="index" name="index" align="right" style="margin-left: 35px" required><br><br>
                            <label for="balance">Balance:</label>
                            <input type="text" id="balance" name="balance" align="right" style="margin-left: 77px" required><br><br>
                            <br>
                            <input type="submit" value="Add balance" name="searchbalance">
                        </form>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                </div>


        <footer class="page-footer fixed-bottom font-small bg-dark ">   
            <div class="container">
                <div class="text-light footer-copyright text-center py-3">© 2020 Copyright:
                    <a href="https://facebook.com/"> Freshman.com</a>
                </div>
            </div>
        </footer>

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    </body>
</html>