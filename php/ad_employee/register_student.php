<?php

session_start();

// check if the user's logged in.
if(!isset($_SESSION['userType'])){
  header("Location: index.php");
    die();
}


// let only admin_employee access student pages.
if($_SESSION['userType'] != 4){
  die();
}

$DATABASE_HOST = 'localhost';
$DATABASE_USER = 'root';
$DATABASE_PASS = '';
$DATABASE_NAME = 'freshman';

$has_errors = 0;
$ERROR ="" ;

$con = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);


 if ( !$con )
    {
        echo 'Unable to connect with database ';
    } 
    else
    {
    if(isset($_POST['add_student'])){
    
    $uname=$_POST['username'];
    $firstname=$_POST['firstname'];
    $lastname=$_POST['lastname'];
    $email=$_POST['email'];
    $phone=$_POST['phone'];
    $password=$_POST['password'];
    $espb=$_POST['espb'];
    $confirm_password=$_POST['confirm_password'];
    $index=$_POST['student_index'];
    $parent=$_POST['parent_name'];
    $status=$_POST['study_status'];
    $date_of_entry=$_POST['date_of_entry'];
    $jmbg=$_POST['jmbg']; 
    $course_id=$_POST['course_id'];
    $year=$_POST['Year'];
   
    if($password!=$confirm_password){
      #die("Passwords do not match");
        $message = "Passwords do not match";
        echo "<script type='text/javascript'>alert('$message');</script>";
    }

    $sql1="select * from fr_users where USERNAME='".$uname."' ";
    
    $result=mysqli_query($con,$sql1);
    $resrows=mysqli_num_rows( $result);

    if( $resrows > 0){
        #die("User with this username already exist");
        $message = "Student with this username already exist";
        echo "<script type='text/javascript'>alert('$message');</script>";
      }

    $sql2="select * from fr_users where EMAIL='".$email."' ";
    
    $result=mysqli_query($con,$sql2);
    $resrows=mysqli_num_rows( $result);

    if( $resrows > 0){
         #die("User with this email already exist");
        $message = "Student with this email already exist";
        echo "<script type='text/javascript'>alert('$message');</script>";
      }
    $sql3="select * from fr_users where JMBG='".$jmbg."' ";
    
    $result=mysqli_query($con,$sql3);
    $resrows=mysqli_num_rows( $result);

    if( $resrows > 0){
           #die("User with this JMBG already exist");
        $message = "Student with this JMBG already exist";
        echo "<script type='text/javascript'>alert('$message');</script>";
    }
    $sql4="select * from fr_users where STUDENT_INDEX='".$index."' ";
    
        $result=mysqli_query($con,$sql4);
        $resrows=mysqli_num_rows( $result);

        if( $resrows > 0){
            #die("Student with this index already exist");
            $message = "Student with this index already exist";
            echo "<script type='text/javascript'>alert('$message');</script>";
      }

          $sql="INSERT INTO fr_users (USERNAME, FIRSTNAME, LASTNAME, EMAIL, PASSWORD, PHONE_NUMBER, ESPB, USER_TYPE, BALANCE, COURSE_ID,STUDENT_INDEX,PARENT_NAME, JMBG, STUDY_STATUS, DATE_OF_ENTRY,YEAR) VALUES ('$uname','$firstname','$lastname','$email','$password','$phone','$espb',3,0,'$course_id','$index','$parent','$jmbg','$status','$date_of_entry','$year')";
    
          if(!mysqli_query($con,$sql)){
              #die("Error inserting new record");
              $message = "Error inserting new record";
              echo "<script type='text/javascript'>alert('$message');</script>";
           }else{
              $message = "Student added successfully";
              echo "<script type='text/javascript'>alert('$message');</script>";
          }
        $has_errors = 0;
    }
    }
?>


<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <link rel="stylesheet" href="../ad_employee.css">

        <title>Ad_employee</title>
    </head>
    <body class="bg-light" >
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <img src="../../images/logo.svg" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="../ad_employee.php">Home Page<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Student register</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="schedule_exam_period.php">Schedule exam period</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="student_balance_changes.php">Funds balance</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="student_pass_year.php">Enrollment and renewal</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="student_pass_exam.php">Exam report</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="notifications.php">Notifications</a>
                    </li>

                </ul>
                <div class="navbar-nav nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php if(isset($_SESSION['username'])){echo ucfirst($_SESSION['username']);}; ?>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="../logout.php">Logout</a>
                    </div>
                </div>
            </div>
        </nav>






        <div class="container " style="text-align: center" >
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <div class="jumbotron">
                        <h3 class="display-5  text-center">Student register</h3>
                        <hr class="my-2">
        <form method="post"  >

            <div class="grid" >

                <div class= "label">
                    <label for="firstname">
                        <i class="fas fa-user" ></i>
                    </label>
                </div>
                <div class="input">
                    <input type="text" name="firstname" placeholder="Firstname"  required>
                </div>

                <div class= "label">
                    <label for="lastname">
                        <i class="fas fa-user"></i>
                    </label>
                </div>
                <div class="input">
                    <input type="text" name="lastname" placeholder="Lastname"  required>
                </div>

                <div class= "label">
                    <label for="email">
                        <i class="fas fa-envelope"></i>
                    </label>
                </div>
                <div class="input">
                    <input type="text" name="email" placeholder="Email"  required>
                </div>

                <div class= "label">
                    <label for="phone">
                        <i class="fas fa-phone"></i>
                    </label>
                </div>
                <div class="input">
                    <input type="text" name="phone" placeholder="Phone number"  required>
                </div>
                <div class= "label">
                    <label for="jmbg">
                        <i class="fas fa-address-card"></i>
                    </label>
                </div>
                <div class="input">
                    <input type="text" name="jmbg" placeholder="JMBG"  required>
                </div>
                <div class= "label">
                    <label for="parent">
                        <i class="fas fa-user-circle"></i>
                    </label>
                </div>
                <div class="input">
                    <input type="text" name="parent_name" placeholder="Name of parent"  required>
                </div>
                <div class= "label">
                    <label for="student_index">
                        <i class="fas fa-info"></i>
                    </label>
                </div>
                <div class="input">
                    <input type="text" name="student_index" placeholder="Student index"  required>
                </div>

                <div class= "label">
                    <label for="study_status">
                        <i class="fas fa-info-circle"></i>
                    </label>
                </div>
                <div class="input">
                    <input type="text" name="study_status" placeholder="Study status"  required>
                </div>

                <div class= "label">
                    <label for="date_of_entry">
                        <i class="fas fa-calendar"></i>
                    </label>
                </div>
                <div class="input">
                    <input type="date" name="date_of_entry"  required>
                </div>

                <div class= "label">
                    <label for="espb">
                        <i class="fas fa-graduation-cap"></i>
                    </label>
                </div>
                <div class="input">
                    <input type="text" name="espb" placeholder="ESPB"  required>
                </div>

                <div class= "label">
                    <label for="course">
                        <i class="fa fa-credit-card" aria-hidden="true"></i>
                    </label>
                </div>
                <div class="input">
                    <input type="text" name="course_id" placeholder="Course"  required>
                </div>

                <div class= "label">
                    <label for="username">
                        <i class="fas fa-user"></i>
                    </label>
                </div>
                <div class="input">
                    <input type="text" name="username" placeholder="Username"  required>
                </div>

                <div class= "label">
                    <label for="password">
                        <i class="fas fa-lock"></i>
                    </label>
                </div>
                <div class="input">
                    <input type="password" name="password" placeholder="Password"  required>
                </div>

                <div class= "label">
                    <label for="password">
                        <i class="fas fa-lock"></i>
                    </label>
                </div>
                <div class="input">
                    <input type="password" name="confirm_password" placeholder="Confirm password"  required>
                </div>
                <div class= "label">
                    <label for="year">
                        <i class="fas fa-lock"></i>
                    </label>
                </div>
                <div class="input">
                    <input type="text" name="Year" placeholder="Year"  required>
                </div>
            </div>
            <br>
            <input type="submit" name="add_student" value="Register student">

        </form>
                    </div>
                </div>
            </div>

        </div></div><br><br><br><br><br>






        <footer class="page-footer fixed-bottom font-small bg-dark ">   
            <div class="container">
                <div class="text-light footer-copyright text-center py-3">© 2020 Copyright:
                    <a href="https://facebook.com/"> Freshman.com</a>
                </div>
            </div>
        </footer>

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    </body>
</html>