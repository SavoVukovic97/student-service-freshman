<?php

session_start();

// check if the user's logged in.
if(!isset($_SESSION['userType'])){
  header("Location: index.php");
    die();
}


// let only admin_employee access student pages.
if($_SESSION['userType'] != 4){
  die();
}

$DATABASE_HOST = 'localhost';
$DATABASE_USER = 'root';
$DATABASE_PASS = '';
$DATABASE_NAME = 'freshman';

$has_errors = 0;
$ERROR ="" ;

$connection = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);


 if ( !$connection )
    {
        echo 'Unable to connect with database ';
    } 
    else
    {
        if(isset($_POST['Schedule'])){
            $name=$_POST['PeriodName'];
            $start=$_POST['StartDate'];
            $end=$_POST['EndDate'];
            $active=$_POST['Active'];

            $query = "INSERT INTO fr_exam_period(EXAM_PERIOD_NAME,EXAM_PERIOD_FROM_DATE,EXAM_PERIOD_TO_DATE,ACTIVE) VALUES ('" . $name . "', '" . $start . "', '" . $end . "', '" . $active . "');";

            $testquerry = "SELECT * FROM fr_exam_period WHERE EXAM_PERIOD_NAME='$name' AND EXAM_PERIOD_FROM_DATE='$start' AND  EXAM_PERIOD_TO_DATE='$end' ";
            $result=mysqli_query($connection,$testquerry);
            $resrows=mysqli_num_rows( $result);

            if( $resrows > 0){
                #die("Period already exists!");
                $message = "Period already exists!";
                echo "<script type='text/javascript'>alert('$message');</script>";
            }elseif(!mysqli_query($connection,$query)){
                #die("Error inserting new record");
                $message = "Error inserting new record";
                echo "<script type='text/javascript'>alert('$message');</script>";
            }
            $has_errors = 0;

        }
        if(isset($_POST['deactivate'])){
            $name=$_POST['PeriodName'];
            $start=$_POST['StartDate'];
            $end=$_POST['EndDate'];
            $active=$_POST['Active'];
            $periodid=$_POST['periodid'];
           # $query1 = "UPDATE fr_exam_period SET ACTIVE=1 WHERE EXAM_PERIOD_NAME='$name' AND EXAM_PERIOD_FROM_DATE='$start' AND EXAM_PERIOD_TO_DATE='$end'";
            $query1 = "UPDATE fr_exam_period SET ACTIVE=1 WHERE EXAM_PERIOD_NAME='$name' AND EXAM_PERIOD_ID=$periodid";
            if(!mysqli_query($connection,$query1)){
                #die("Error changing active status");
                $message = "Error changing active status";
                echo "<script type='text/javascript'>alert('$message');</script>";
            }
        }
    }
?>


<!doctype html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

        <title>Ad_employee</title>
    </head>
    <body class="bg-light">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <img src="../../images/logo.svg" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="../ad_employee.php">Home Page<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="register_student.php">Student register</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Schedule exam period</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="student_balance_changes.php">Funds balance</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="student_pass_year.php">Enrollment and renewal</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="student_pass_exam.php">Exam report</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="notifications.php">Notifications</a>
                    </li>

                </ul>
                <div class="navbar-nav nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php if(isset($_SESSION['username'])){echo ucfirst($_SESSION['username']);}; ?>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="../logout.php">Logout</a>
                    </div>
                </div>
            </div>
        </nav>

        <div class="container" style="text-align: center">
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <div class="jumbotron">
                        <h3 class="display-5  text-center">Schedule exam</h3>
                        <hr class="my-4">

                        <form method="post" >
                        <label for="PeriodName" style="margin-right: 40px">Exam period name:</label>
                        <input type="text" id="PeriodName" name="PeriodName" align="right" ><br><br>
                            <label for="periodid" style="margin-right: 65px">Exam period id:</label>
                            <input type="text" id="periodid" name="periodid" align="right" ><br><br>
                        <label for="StartDate" style="margin-right: 85px">Start date:</label>
                        <input type=date id="StartDate" name="StartDate" ><br><br>
                        <label for="EndDate"style="margin-right: 90px">End date:</label>
                        <input type=date id="EndDate" name="EndDate" ><br><br>
                        <label for="Active" style="margin-right: 100px">Active period:</label>
                        <select name='Active' id='Active' style="margin-left: 85px">
                            <option value='1'>No</option>
                            <option value='0'>Yes</option>
                        </select><br><br>
                        <input type="submit" value="Schedule" name="Schedule" style="margin-right: 170px">
                        <input type="submit" value="Deactivate" name="deactivate">
                        </form>

                    </div>
                </div>
            </div>

        </div>




        <div class="container" style="text-align: center">
            <?php $results = mysqli_query($connection, "SELECT * FROM fr_exam_period  "); ?>

            <div class="row">
                <table class="table table-light">
                    <thead class="thead-dark">
                    <tr>
                        <th>Id</th>
                        <th>Period name</th>
                        <th>Start date</th>
                        <th>End date</th>
                        <th>Active</th>

                    </tr>
                    </thead>

                    <tbody>
                    <?php while ($row = mysqli_fetch_array($results)) { ?>
                        <tr>
                            <td><?php echo $row['EXAM_PERIOD_ID']; ?></td>
                            <td><?php echo $row['EXAM_PERIOD_NAME']; ?></td>
                            <td><?php echo $row['EXAM_PERIOD_FROM_DATE']; ?></td>
                            <td><?php echo $row['EXAM_PERIOD_TO_DATE']; ?></td>
                            <td><?php echo $row['ACTIVE']; ?></td>

                        </tr>
                    <?php } ?>
                    </tbody>
                </table>

            </div>
        </div>
        <br><br><br><br><br><br><br>

        <footer class="page-footer fixed-bottom font-small bg-dark ">   
            <div class="container">
                <div class="text-light footer-copyright text-center py-3">© 2020 Copyright:
                    <a href="https://facebook.com/"> Freshman.com</a>
                </div>
            </div>
        </footer>

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    </body>
</html>