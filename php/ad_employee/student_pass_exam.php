<?php

session_start();

// check if the user's logged in.
if(!isset($_SESSION['userType'])){
  header("Location: index.php");
    die();
}


// let only admin_employee access student pages.
if($_SESSION['userType'] != 4){
  die();
}

$DATABASE_HOST = 'localhost';
$DATABASE_USER = 'root';
$DATABASE_PASS = '';
$DATABASE_NAME = 'freshman';

$has_errors = 0;
$ERROR ="" ;

$connection = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);


if ( !$connection )
{
    echo 'Unable to connect with database ';
}
else
{
    if(isset($_POST['Passed'])){

        $studentindex = $_POST['index'];
        $examid=$_POST['examid'];

        $query = "SELECT ID FROM fr_users WHERE STUDENT_INDEX='$studentindex'";

        $result = mysqli_query($connection, $query);
        $resrows=mysqli_num_rows( $result);

        if( $resrows > 0) {
            while ($user_info = mysqli_fetch_row($result)) {
                $id = $user_info[0];
            }

            $testquery = "SELECT EXAM_ID FROM fr_exams WHERE EXAM_ID=$examid";
            $result1 = mysqli_query($connection, $testquery);
            $resrows1=mysqli_num_rows( $result1);
            if( $resrows1 > 0) {

                $testingquerry1 = "SELECT * FROM fr_user_exams WHERE EXAM_ID='$examid' AND ID='$id' AND PASSED=0 and GRADE>5";
                $result2 = mysqli_query($connection, $testingquerry1);
                $resrows2=mysqli_num_rows( $result2);

                if( $resrows2 > 0){
                    $query2 = "UPDATE fr_user_exams SET PASSED=1 WHERE EXAM_ID='$examid' AND ID='$id'";
                    $insert = mysqli_query($connection, $query2);

                    $query3 = "SELECT ESPB FROM fr_exams WHERE EXAM_ID=$examid";
                    # $insert1 = mysqli_query($connection, $query3);
                    $result3 = mysqli_query($connection, $query3);
                    while ($user_info3 = mysqli_fetch_row($result3)) {
                        $espb = $user_info3[0];
                    }

                    $query4 = "UPDATE fr_users SET ESPB=ESPB+$espb WHERE STUDENT_INDEX=$studentindex";
                    #$insert2 = mysqli_query($connection,$query4);

                    if (!mysqli_query($connection, $query4)) {
                        $message = "Error passing student $studentindex exam $examid";
                        echo "<script type='text/javascript'>alert('$message');</script>";
                    } else {
                        $message = "Student $studentindex exam $examid successfully updated";
                        echo "<script type='text/javascript'>alert('$message');</script>";
                    }
                }else{
                    $message = "Student $studentindex does not have exam $examid registered";
                    echo "<script type='text/javascript'>alert('$message');</script>";
                }


            }else{
                $message = "Error passing student $studentindex exam $examid , invalid exam id!";
                echo "<script type='text/javascript'>alert('$message');</script>";
            }

        }else{
            $message = "Error passing student $studentindex exam $examid , invalid index!";
            echo "<script type='text/javascript'>alert('$message');</script>";
        }
    }
}
?>


<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

        <title>Ad_employee</title>
    </head>
    <body class="bg-light">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <img src="../../images/logo.svg" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="../ad_employee.php">Home Page<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="register_student.php">Student register</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="schedule_exam_period.php">Schedule exam period</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="student_balance_changes.php">Funds balance</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="student_pass_year.php">Enrollment and renewal</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Exam report</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="notifications.php">Notifications</a>
                    </li>

                </ul>
                <div class="navbar-nav nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php if(isset($_SESSION['username'])){echo ucfirst($_SESSION['username']);}; ?>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="../logout.php">Logout</a>
                    </div>
                </div>
            </div>
        </nav>



        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <div class="jumbotron">
                        <h3 class="display-7  text-center">Enter student index</h3>
                        <hr class="my-4">
                        <br>
                        <form method="post" >
                            <label for="index" style="margin-right: 30px">Student index:</label>
                            <input type="text" id="index" name="index" align="right" required><br><br>
                            <label for="examid" style="margin-right: 72px">Exam id:</label>
                            <input type="text" id="examid" name="examid" align="right" required><br><br>
                            <input type="submit" value="Pass" name="Passed">
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <?php $results = mysqli_query($connection, "SELECT * FROM fr_user_exams WHERE PASSED=0 AND GRADE>5 ORDER BY EXAM_ID"); ?>

            <table class="table table-light">
                <thead class="thead-dark">
                <tr>
                    <th>Exam id</th>
                    <th>Firstname</th>
                    <th>Lastname</th>
                    <th>Student index</th>
                    <th>Passed</th>
                    <th>Year passed</th>
                    <th>Grade</th>
                    <th>Professor</th>
                    <th>Exam aplications</th>
                    <th>Exam period</th>



                </tr>
                </thead>

                <?php while ($row = mysqli_fetch_array($results)) {
                        $variable = $row['ID'];
                        $querryidindex = "SELECT STUDENT_INDEX,FIRSTNAME,LASTNAME FROM fr_users WHERE ID=$variable";
                        $result6 = mysqli_query($connection, $querryidindex);
                        while($user_info1 = mysqli_fetch_row($result6)){
                                    $studentindex=$user_info1[0];
                                    $studentname =$user_info1[1];
                                    $studentlastname =$user_info1[2];
                            }
                        $variable2 = $row['EXAM_PERIOD_ID'];
                        $querryexamid = "SELECT EXAM_PERIOD_NAME FROM fr_exam_period WHERE EXAM_PERIOD_ID=$variable2";
                        $examidresults = mysqli_query($connection, $querryexamid);
                        while($user_info2 = mysqli_fetch_row($examidresults)){
                                    $examname=$user_info2[0];
                                    
                            }
                    ?>

                    <tr>
                        <td><?php echo $row['EXAM_ID']; ?></td>
                        <td><?php echo $studentname; ?></td>
                        <td><?php echo $studentlastname; ?></td>
                        <td><?php echo $studentindex; ?></td>
                        <td><?php echo $row['PASSED']; ?></td>
                        <td><?php echo $row['PASSING_YEAR'] ?></td>
                        <td><?php echo $row['GRADE']; ?></td>
                        <td><?php echo $row['PROFESSOR']; ?></td>
                        <td><?php echo $row['EXAM_APPLICATIONS']; ?></td>
                        <td><?php echo $examname; ?></td>


                    </tr>
                <?php } ?>
            </table></div>
            <br><br><br><br><br><br>


        <footer class="page-footer fixed-bottom font-small bg-dark ">   
            <div class="container">
                <div class="text-light footer-copyright text-center py-3">© 2020 Copyright:
                    <a href="https://facebook.com/"> Freshman.com</a>
                </div>
            </div>
        </footer>

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    </body>
</html>