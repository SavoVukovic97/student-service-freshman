<?php 
session_start();
// Allow only admins to access this page.
if($_SESSION['userType'] != 1){
  die();
}

$DATABASE_HOST = 'localhost';
$DATABASE_USER = 'root';
$DATABASE_PASS = '';
$DATABASE_NAME = 'freshman';

$has_errors = 0;
$ERROR ="" ;

$con = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);

if ( !$con ) {
 
  echo 'Unable to connect with database ';
}

else{
  if (isset($_GET['edit'])) {
    $id = $_GET['edit'];
    $update = true;
    $record = mysqli_query($con, "SELECT * FROM fr_users WHERE ID=$id");
    $resrows=mysqli_num_rows( $record);

    if ($resrows == 1 ) {
      $n = mysqli_fetch_array($record);
      $username = $n['USERNAME'];
      $firstname = $n['FIRSTNAME'];
      $lastname = $n['LASTNAME'];
      $password=$n['PASSWORD'];
      $email = $n['EMAIL'];
      $phone = $n['PHONE_NUMBER'];
      $user_type = $n['USER_TYPE'];
      $balance = $n['BALANCE'];
      $espb = $n['ESPB'];
      $index=$n['STUDENT_INDEX'];
  $parent=$n['PARENT_NAME'];
  $status=$n['STUDY_STATUS'];
  $date_of_entry=$n['DATE_OF_ENTRY'];
  $jmbg=$n['JMBG']; 
  $course_id=$n['COURSE_ID'];
  $year=$n['YEAR'];
    }
  }
  else{
    die();
  }
if (isset($_POST['update_student'])) {
  
 
  $username = $_POST['username'];
  $firstname = $_POST['firstname'];
  $lastname = $_POST['lastname'];
  $password=$_POST['password'];
  $email = $_POST['email'];
  $phone = $_POST['phone'];      
  $balance = $_POST['cash'];
  $espb = $_POST['espb'];
  $index=$_POST['student_index'];
  $parent=$_POST['parent_name'];
  $status=$_POST['study_status'];
  $date_of_entry=$_POST['date_of_entry'];
  $jmbg=$_POST['jmbg']; 
  $course_id=$_POST['course_id'];
  $year=$_POST['year'];

  mysqli_query($con, "UPDATE fr_users SET USERNAME='$username', FIRSTNAME='$firstname', LASTNAME='$lastname', EMAIL='$email', PASSWORD='$password', PHONE_NUMBER='$phone', ESPB='$espb', USER_TYPE=3, BALANCE='$balance', STUDENT_INDEX='$index', PARENT_NAME='$parent', STUDY_STATUS='$status', DATE_OF_ENTRY='$date_of_entry', JMBG='$jmbg', COURSE_ID='$course_id', YEAR='$year' WHERE ID=$id");
  $_SESSION['message'] = "Student successfully updated!"; 
 
}
if (isset($_POST['update_professor'])) {
  
  
  $username = $_POST['username'];
  $firstname = $_POST['firstname'];
      $lastname = $_POST['lastname'];
      $password=$_POST['password'];
      $email = $_POST['email'];
      $phone = $_POST['phone'];
      $parent=$_POST['parent_name'];
  $jmbg=$_POST['jmbg'];


  mysqli_query($con, "UPDATE fr_users SET USERNAME='$username', FIRSTNAME='$firstname', LASTNAME='$lastname', EMAIL='$email', PASSWORD='$password', PHONE_NUMBER='$phone', JMBG='$jmbg', PARENT_NAME='$parent'/*, ESPB=0, USER_TYPE=2, BALANCE=0 */WHERE ID=$id");
  $_SESSION['message2'] = "Professor successfully updated!"; 
 
}
if (isset($_POST['update_employee'])) {
  
  
  $username = $_POST['username'];
  $firstname = $_POST['firstname'];
  $lastname = $_POST['lastname'];
  $password=$_POST['password'];
  $email = $_POST['email'];
  $phone = $_POST['phone'];
  $parent=$_POST['parent_name'];
  $jmbg=$_POST['jmbg'];


  mysqli_query($con, "UPDATE fr_users SET USERNAME='$username', FIRSTNAME='$firstname', LASTNAME='$lastname', EMAIL='$email', PASSWORD='$password', PHONE_NUMBER='$phone', JMBG='$jmbg', PARENT_NAME='$parent'/*, ESPB=0, USER_TYPE=2, BALANCE=0 */WHERE ID=$id");
  $_SESSION['message3'] = "Administrative employee updated!"; 
 
}
}
?>



<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Admin</title>
    <link rel="stylesheet" href="../css/admin1234.css">
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
  </head>
  <body>

<!--FOR UPDATING STUDENT-->

    <div class="content111" <?php if($user_type==3) {echo 'style="display:block;"';} ?> >
      <div class="login">
      <div class="logo">
        <img src="../images/logo.svg" style="width: 18%;">
      </div>
      <h1>Update student</h1>
     
      <?php if (isset($_SESSION['message'])): ?>
		<div class="msg">
			<?php 
			echo $_SESSION['message']; 
			unset($_SESSION['message']);
			?>
		</div>
	<?php endif ?>
      <form method="post">
        <div class="grid " >

        <div class= "label">
          <label for="firstname">
            <i class="fas fa-user"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="firstname" placeholder="Firstname"
          value="<?php echo $firstname; ?>"  required>
         </div> 
          
        <div class= "label">
          <label for="lastname">
            <i class="fas fa-user"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="Lastname"  required>
         </div> 
          
         <div class= "label">
          <label for="email">
            <i class="fas fa-envelope"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="email" placeholder="Email" value="<?php echo $email; ?>" required>
         </div> 

         <div class= "label">
          <label for="phone">
            <i class="fas fa-phone"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="phone" placeholder="Phone number"  value="<?php echo $phone; ?>"required>
         </div> 

         <div class= "label">
          <label for="espb">
            <i class="fas fa-graduation-cap"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="espb" placeholder="ESPB"  value="<?php echo $espb; ?>" required>
         </div> 

        <div class= "label">
          <label for="year">
            <i class="fas fa-graduation-cap"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="year" placeholder="YEAR"  value="<?php echo $year; ?>" required>
         </div>         

         <div class= "label">
          <label for="cash">
            <i class="fa fa-credit-card" aria-hidden="true"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="cash" placeholder="Cash" value="<?php echo $balance; ?>" id="cash" required>
         </div> 

         <div class= "label">
          <label for="jmbg">
            <i class="fas fa-address-card"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="jmbg" placeholder="JMBG" value="<?php echo $jmbg; ?>"  required>
         </div> 
         <div class= "label">
          <label for="parent">
            <i class="fas fa-user-circle"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="parent_name" placeholder="Name of parent" value="<?php echo $parent; ?>"  required>
         </div> 
        <div class= "label">
          <label for="student_index">
            <i class="fas fa-info"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="student_index" placeholder="Student index" value="<?php echo $index; ?>"  required>
         </div> 
         
         <div class= "label">
          <label for="study_status">
            <i class="fas fa-info-circle"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="study_status" placeholder="Study status" value="<?php echo $status; ?>"  required>
         </div> 
         
         <div class= "label">
          <label for="date_of_entry">
            <i class="fas fa-calendar"></i>
          </label>
        </div>
        <div class="input">
          <input type="date" name="date_of_entry"  value="<?php echo $date_of_entry; ?>"  required>
         </div> 

         <div class= "label">
          <label for="course">
            <i class="fa fa-credit-card" aria-hidden="true"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="course_id" placeholder="Course" value="<?php echo $course_id; ?>"  required>
         </div> 


        <div class= "label">
          <label for="username">
            <i class="fas fa-user"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="username" placeholder="Username" value="<?php echo $username; ?>"  required>
         </div> 

         <div class= "label">
          <label for="password">
            <i class="fas fa-lock"></i>
          </label>
        </div>
        <div class="input">
        <input type="text" name="password" value="<?php echo $password; ?>" placeholder="Password"  required>
      </div>

      
      </div>

        <input type="submit" name="update_student" value="Update">

      </form></div></div>




<!--FOR UPDATING PROFESSOR-->

    <div class="content3" <?php if($user_type==2) {echo 'style="display:block;"';} ?>>
      <div class="login">
      <div class="logo">
        <img src="../images/logo.svg" style="width: 30%;">
      </div>
      <h1>Add professor</h1>

      <?php if (isset($_SESSION['message2'])): ?>
		<div class="msg">
		<?php 
			echo $_SESSION['message2']; 
			unset($_SESSION['message2']);
		?>
	</div>
<?php endif ?>
      <form method="post">
        <div class="grid">

        <div class= "label">
          <label for="firstname">
            <i class="fas fa-user"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="firstname" placeholder="Firstname" value="<?php echo $firstname; ?>"  required>
         </div> 
          
        <div class= "label">
          <label for="lastname">
            <i class="fas fa-user"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="lastname" placeholder="Lastname"  value="<?php echo $lastname; ?>" required>
         </div> 
          
         <div class= "label">
          <label for="email">
            <i class="fas fa-envelope"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="email" placeholder="Email" value="<?php echo $email; ?>" required>
         </div> 

         <div class= "label">
          <label for="phone">
            <i class="fas fa-phone"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="phone" placeholder="Phone number" value="<?php echo $phone; ?>" required>
         </div> 


         <div class= "label">
          <label for="jmbg">
            <i class="fas fa-address-card"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="jmbg" placeholder="JMBG" value="<?php echo $jmbg; ?>"  required>
         </div> 
         <div class= "label">
          <label for="parent">
            <i class="fas fa-user-circle"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="parent_name" placeholder="Name of parent" value="<?php echo $parent; ?>"  required>
         </div> 

        <div class= "label">
          <label for="username">
            <i class="fas fa-user"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="username" placeholder="Username" value="<?php echo $username; ?>"  required>
         </div> 

         <div class= "label">
          <label for="password">
            <i class="fas fa-lock"></i>
          </label>
        </div>
        <div class="input">
        <input type="text" name="password" placeholder="Password" value="<?php echo $password; ?>" required>
      </div>

      
      </div>

        <input type="submit" name="update_professor" value="Update">

      </form></div></div>

<!--FOR UPDATING ADMINISTRATIVE EMPLOYEE-->

    <div class="content4" <?php if($user_type==4) {echo 'style="display:block;"';} ?>>
      <div class="login">
      <div class="logo">
        <img src="../images/logo.svg" style="width: 30%;">
      </div>
      <h1>Add administrative employee</h1>
      <p class="error" <?php if($has_errors) {echo 'style="border:1.5px solid red;"';} ?>><?php if($has_errors) {echo $ERROR ;} ?></p>
      <?php if (isset($_SESSION['message3'])): ?>
	<div class="msg">
		<?php 
			echo $_SESSION['message3']; 
			unset($_SESSION['message3']);
		?>
	</div>
<?php endif ?>
      <form method="post">
        <div class="grid">

        <div class= "label">
          <label for="firstname">
            <i class="fas fa-user"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="firstname" placeholder="Firstname" value="<?php echo $firstname; ?>" required>
         </div> 
          
        <div class= "label">
          <label for="lastname">
            <i class="fas fa-user"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="lastname" placeholder="Lastname" value="<?php echo $lastname; ?>"  required>
         </div> 
          
         <div class= "label">
          <label for="email">
            <i class="fas fa-envelope"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="email" placeholder="Email" value="<?php echo $email; ?>" required>
         </div> 

         <div class= "label">
          <label for="phone">
            <i class="fas fa-phone"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="phone" placeholder="Phone number"  value="<?php echo $phone; ?>" required>
         </div> 

         <div class= "label">
          <label for="jmbg">
            <i class="fas fa-address-card"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="jmbg" placeholder="JMBG" value="<?php echo $jmbg; ?>"  required>
         </div> 
         <div class= "label">
          <label for="parent">
            <i class="fas fa-user-circle"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="parent_name" placeholder="Name of parent" value="<?php echo $parent; ?>"  required>
         </div> 

        <div class= "label">
          <label for="username">
            <i class="fas fa-user"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="username" value="<?php echo $username; ?>" placeholder="Username"  required>
         </div> 

         <div class= "label">
          <label for="password">
            <i class="fas fa-lock"></i>
          </label>
        </div>
        <div class="input">
        <input type="text" name="password" value="<?php echo $password; ?>" placeholder="Password"  required>
      </div>

      
      </div>

        <input type="submit" name="update_employee" value="Update">

      </form></div></div>
<a href="admin.php" style="position: absolute;
    bottom: 0px;
    margin: 5px;
    color: #2a2acc;">Go back to admin page</a>
</body>
</html>