<?php

session_start();

if(!isset($_SESSION['userType'])){
    header("Location: index.php");
    die();
}

if($_SESSION['userType'] != 3){
    die();
}

$DATABASE_HOST = 'localhost';
$DATABASE_USER = 'root';
$DATABASE_PASS = '';
$DATABASE_NAME = 'freshman';

$has_errors = 0;
$ERROR ="" ;

$connection = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);

$userId = $_POST['userId'];
$examId = $_POST['examId'];
$periodId = $_POST["periodId"];

$query = "INSERT INTO fr_user_exam_registration(EXAM_ID,ID,EXAM_PERIOD_ID) VALUES (" . $examId . "," . $userId . "," . $periodId . ")";

$result = mysqli_query($connection, $query);
if(!$result){
    die("Error on exam registration!");
} else {
    if(isset($_POST['withdraw']) && $_POST['withdraw'] == 1){
        $query = "UPDATE fr_users SET BALANCE = BALANCE - 1000 WHERE USERNAME = '" . $_SESSION['username'] . "'";
        echo $query;
        $result = mysqli_query($connection, $query);
        if(!$result){
            die("Error on exam registration!");
        }
    } else {
        $query = "UPDATE fr_user_exams SET EXAM_APPLICATIONS = EXAM_APPLICATIONS - 1 WHERE EXAM_ID = " . $examId;
        $result = mysqli_query($connection, $query);
        if(!$result){
            die("Error on exam registration!");
        }
    }
}

header("Location: ../student.php");
