<?php

session_start();

if(!isset($_SESSION['userType'])){
    header("Location: index.php");
    die();
}

if($_SESSION['userType'] != 3){
    die();
}

if(!isset($_POST['examName'])){
    die();
}

$DATABASE_HOST = 'localhost';
$DATABASE_USER = 'root';
$DATABASE_PASS = '';
$DATABASE_NAME = 'freshman';

$has_errors = 0;
$ERROR ="" ;

$connection = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);

// it's unique so it's ok to search by the exam name.
$examName = $_POST['examName'];
// current active period, admin must make sure that there's only one active registration period at a time
$periodId = $_POST['periodId'];

$query = "SELECT exam.MANDATORY, exam.ESPB, period.EXAM_PERIOD_NAME, user.FIRSTNAME, user.LASTNAME,user.ID, exam.EXAM_ID FROM fr_exams exam JOIN fr_exam_period period JOIN fr_users user WHERE exam.EXAM_NAME = '" . $examName . "' AND period.ACTIVE = 0 AND user.USERNAME = '" . $_SESSION['username'] . "'";

$result = mysqli_query($connection, $query);

$mandatory = 0;
$points = 0;
$period = "Unknown";
$firstName = "Unknown";
$lastName = "Unknown";
$userId = 0;
$examId = 0;
while($info = mysqli_fetch_row($result)){
    $mandatory = $info[0];
    $points = $info[1];
    $period = ucfirst($info[2]);
    $firstName = ucfirst($info[3]);
    $lastName = ucfirst($info[4]);
    $userId = $info[5];
    $examId = $info[6];
}

$attempts = $_POST['attempts'];
$balance = $_POST['balance'];

$canRegisterExam = 1;
$message = "";
if($attempts <= 0) {
    if($balance < 1000){
        $canRegisterExam = 0;
        $message = "You don't have enough founds to register this exam!";
    } else { 
        $message = "This registration will withdraw 1000 din from your account!";
    }
}



?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Exams</title>
  </head>
  <body class="bg-light">
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <img src="../../images/logo.svg" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
  </a>

  <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="../student.php">Home Page</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="./exams.php">Exams</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="exam-registration.php">Exam Registration</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="registered-exams.php">Registered Exams</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="upload-docs.php">Upload Documentation</a>
            </li>
        </ul>
        <ul class="navbar-nav">
        <div class="navbar-nav nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="false" aria-expanded="true">
            <?php if(isset($_SESSION['username'])){echo ucfirst($_SESSION['username']);}; ?>
             </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="../logout.php">Logout</a>
            </div>
        </div>
        </ul>
 </div>
</nav>
<br>
<div class="container">
    <div class="row d-flex justify-content-xl-center">
        
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
        
        <?php 
            if($attempts <= 0){
                echo '<div class="alert alert-primary" role="alert">' . $message . "</div>";
            }
        
        ?>
        <form action="submit-exam.php" method="POST">
            <div class="form-group">
                <label for="userInfo">User's full name</label>
                <input type="text" readonly='true' class="form-control" id="userInfo" value='<?php echo $firstName . " " . $lastName; ?>'>
                <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
            </div>
            <div class="form-group">
                <label for="examName">Exam name</label>
                <input type="text" readonly='true' class="form-control" id="examName" value='<?php echo $examName; ?>'>
            </div>
            <div class="form-group">
                <label for="examPeriod">Exam period</label>
                <input type="text" readonly='true' class="form-control" id="examPeriod" value='<?php echo $period; ?>'>
            </div>
            <div class="form-group">
                <label for="ESPB">ESPB</label>
                <input type="text" readonly='true' class="form-control" id="ESPB" value='<?php echo $points; ?>'>
            </div>
            <div class="form-group">
                <label for="mandatory">Mandatory</label>
                <input type="text" readonly='true' class="form-control" id="mandatory" value='<?php echo $mandatory == 0 ? "Yes" : "No"?>'>                
            </div>

            <input type="hidden" class="form-control" name="periodId" value='<?php echo $periodId; ?>' >
            <input type="hidden" class="form-control" name="examId" value='<?php echo $examId; ?>' >
            <input type="hidden" class="form-control" name="userId" value='<?php echo $userId; ?>' >
            <?php 
                if($canRegisterExam == 1) {
                    echo '<button type="submit" class="btn btn-success">Submit Exam Registration</button>';
                    if($attempts <= 0){
                        echo '<input type="hidden" class="form-control" name="withdraw" value="1" >';
                    }
                } else {
                    echo '<button type="submit" class="btn btn-success" disabled>Submit Exam Registration</button>';
                }
            ?>
        </form>
        </div>
    </div>
</div>

<footer class="page-footer fixed-bottom font-small bg-dark ">   
    <div class="container">
        <div class="text-light footer-copyright text-center py-3">© 2020 Copyright:
            <a href="https://facebook.com/"> Freshman.com</a>
        </div>
    </div>
</footer>

    



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>