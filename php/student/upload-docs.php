<?php

session_start();

if(!isset($_SESSION['userType'])){
    header("Location: index.php");
    die();
}

if($_SESSION['userType'] != 3){
    die();
}

$uploadFileAttempt = 0; 
if(isset($_GET['success'])){
    $uploadFileAttempt = 1;
}

$success = "";
$filename = "";
$message = "";
if($uploadFileAttempt == 1){
    $success = $_GET['success'];    
    $filename = $_GET['filename'];
    if($success == 1){
        $message = "File " . $filename . " was successfully uploaded!";
    } else {
        $message = "File " . $filename . " was not uploaded! Your file is either too large or not supported."; 
    }
}

?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Update Documentation</title>
  </head>
  <body class="bg-light">
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <img src="../../images/logo.svg" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
  </a>

  <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="../student.php">Home Page</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="./exams.php">Exams</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="exam-registration.php">Exam Registration</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="registered-exams.php">Registered Exams</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="#">Upload Documentation</a>
            </li>
        </ul>
        <ul class="navbar-nav">
        <div class="navbar-nav nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="false" aria-expanded="true">
            <?php if(isset($_SESSION['username'])){echo ucfirst($_SESSION['username']);}; ?>
             </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="../logout.php">Logout</a>
            </div>
        </div>
        </ul>
 </div>
</nav>

<div class="container p-3">
    <?php 
        if($uploadFileAttempt == 1){
            if($success == 1){
                echo '<div class="alert alert-success" role="alert">' . $message . '</div>';
            } else {
                echo '<div class="alert alert-danger" role="alert">' . $message . '</div>';
            }
        } 
    ?>
    <div class="jumbotron">
        <p class="font-weight-bold">Upload an images of paid exam registrations</p>
        <form action="upload.php" method="post" enctype="multipart/form-data">
        Select image to upload:
        <input class="form-controll" type="file" name="fileToUpload" id="fileToUpload">
        <button class="btn btn-primary d-block mt-3" type="submit" name="Submit">Upload Image</button>        
    </div>
</div>




<footer class="page-footer fixed-bottom font-small bg-dark ">   
    <div class="container">
        <div class="text-light footer-copyright text-center py-3">© 2020 Copyright:
            <a href="https://facebook.com/"> Freshman.com</a>
        </div>
    </div>
</footer>

    



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>