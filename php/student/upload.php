<?php


session_start();

if(!isset($_SESSION['userType'])){
  header("Location: index.php");
  die();
}

if($_SESSION['userType'] != 3){
  die();
}

$target_dir = "../../images/user-uploads/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

$DATABASE_HOST = 'localhost';
$DATABASE_USER = 'root';
$DATABASE_PASS = '';
$DATABASE_NAME = 'freshman';

$connection = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);

if(!$connection){
   header("Location: upload-docs.php?success=0&filename=" . basename( $_FILES["fileToUpload"]["name"]));
}
 
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
  $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
  if($check !== false) {
    $uploadOk = 1;
  } else {
    $uploadOk = 0;
  }
}

// Check if file already exists
if (file_exists($target_file)) {
  $uploadOk = 0;
}

// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000) {
    $uploadOk = 0;
}

// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
    $uploadOk = 0;
}

$filename = basename( $_FILES["fileToUpload"]["name"]);
$message = "File " . $filename . " has been uploaded!";

$query = "INSERT INTO fr_user_upload_activity(USER_ID, MESSAGE_CONTENT, FILE_NAME) SELECT ID, '" .  $message  . "','" . $filename . "' FROM fr_users WHERE USERNAME = '" . $_SESSION['username'] . "'";


// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    header("Location: upload-docs.php?success=0&filename=" . basename( $_FILES["fileToUpload"]["name"]));
// if everything is ok, try to upload file
} else {
  if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
    $result = mysqli_query($connection, $query);
    if(!$result){
      die("There was a problem while trying to upload the file!");
    }
    header("Location: upload-docs.php?success=1&filename=" . basename( $_FILES["fileToUpload"]["name"]));
    
  } else {
    header("Location: upload-docs.php?success=0&filename=" . basename( $_FILES["fileToUpload"]["name"]));
  }
}

?>