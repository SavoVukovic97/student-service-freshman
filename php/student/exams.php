<?php

include "./models/exam.php";

session_start();

if(!isset($_SESSION['userType'])){
    header("Location: index.php");
    die();
}

if($_SESSION['userType'] != 3){
    die();
}

$DATABASE_HOST = 'localhost';
$DATABASE_USER = 'root';
$DATABASE_PASS = '';
$DATABASE_NAME = 'freshman';

$has_errors = 0;
$ERROR ="" ;

$connection = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);

$showPassedExams = false;
if(isset($_POST['buttonSubmit'])){
    $showPassedExams = true;
}

$disableFilters = false;
if(isset($_POST['showAllButton'])){
    $disableFilters = true;
}

$exams = array();

if ( !$connection ) {
  echo 'Unable to connect with database ';
} else { 
  $query = "SELECT exam.EXAM_NAME, exam.EXAM_ID, exam.MANDATORY, exam.ESPB, user_exam.PASSED, user_exam.PROFESSOR FROM fr_user_exams user_exam INNER JOIN fr_users user ON user.ID = user_exam.ID INNER JOIN fr_exams exam ON user_exam.EXAM_ID = exam.EXAM_ID WHERE USERNAME = '" . $_SESSION['username'] . "'";
  
  if($showPassedExams && !$disableFilters){
      $query .= " AND user_exam.PASSED = 1";
  }
  $result = mysqli_query($connection, $query);
  while($exam_info = mysqli_fetch_row($result)){
    $exam = new Exam;
    $exam->examName = ucfirst($exam_info[0]);
    $exam->examId = $exam_info[1];
    $exam->mandatory = $exam_info[2];
    $exam->espb = $exam_info[3];
    $exam->passed = $exam_info[4];
    $exam->professor = $exam_info[5];

    $exams[] = $exam;

  }
}


?>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Exams</title>
  </head>
  <body class="bg-light">
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <img src="../../images/logo.svg" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
  </a>

  <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="../student.php">Home Page<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="#">Exams <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="exam-registration.php">Exam Registration</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="registered-exams.php">Registered Exams</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="upload-docs.php">Upload Documentation</a>
            </li>
        </ul>
        <ul class="navbar-nav">
        <div class="navbar-nav nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="false" aria-expanded="true">
            <?php if(isset($_SESSION['username'])){echo ucfirst($_SESSION['username']);}; ?>
             </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="../logout.php">Logout</a>
            </div>
        </div>
        </ul>
 </div>
</nav>
<br>
<div class="container">
    <div class="row">
        <table class="table table-light">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Exam Name</th>
                    <th scope="col">ESPB</th>
                    <th scope="col">Mandatory</th>
                    <th scope="col">Passed</th>
                    <th scope="col">Professor</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $count = 1; 
                    foreach($exams as $exam){
                        $passedIcon = "<h4><span class='badge badge-danger'>No</span></h4>";
                        if($exam->passed == 1){
                            $passedIcon = "<h4><span class='badge badge-success'>Yes</span></h4>";
                        }
                        $mandatoryIcon = "<h4><span class='badge badge-danger'>No</span></h4>";
                        if($exam->mandatory == 0){
                            $mandatoryIcon = "<h4><span class='badge badge-success'>Yes</span></h4>";
                        }

                        echo "<tr>";
                        echo "<th scope='row'>" . $count++ . "</td>";    
                        echo "<td>" . $exam->examName . "</td>";
                        echo "<td>" . $exam->espb . "</td>";
                        echo "<td>" . $mandatoryIcon . "</td>";
                        echo "<td>" . $passedIcon . "</td>";
                        echo "<td>" . $exam->professor . "</td>";
                        echo "</tr>";
                    }            
                ?>
            </tbody>
        </table>
        
    </div>
    <div class="row float-right">
    <form class="pr-3" method="post" action="exams.php">
        <input hidden='true' class="form-control" name="showAllButton" id="showAllButton">
        <button type="submit" class="btn btn-success">Show all</button>
    </form>
    <form method="post" action="exams.php">
        <input hidden='true' class="form-control" name="buttonSubmit" id="buttonSubmit">
        <button type="submit" class="btn btn-success">Show only passed exams</button>
    </form>
    </div>
</div>


<footer class="page-footer fixed-bottom font-small bg-dark ">   
    <div class="container">
        <div class="text-light footer-copyright text-center py-3">© 2020 Copyright:
            <a href="https://facebook.com/"> Freshman.com</a>
        </div>
    </div>
</footer>

    



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>