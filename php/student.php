<?php 

session_start();

// check if the user's logged in.
if(!isset($_SESSION['userType'])){
  header("Location: index.php");
}


// let only students access student pages.
if($_SESSION['userType'] != 3){
  die();
}

$DATABASE_HOST = 'localhost';
$DATABASE_USER = 'root';
$DATABASE_PASS = '';
$DATABASE_NAME = 'freshman';

$has_errors = 0;
$ERROR ="" ;

$connection = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);

$firstName = "Unknown";
$lastName = "Unknown";
$parentName = "Unknown";
$index = "";
$email = "Unknown";
$phoneNumber = "Unknown";
$index = "Unknown";
$dateOfEntry = "Unknown";
$courseName = "Unknown";
$courseESPB = 0;
$balance = 0;

$userid = 0;

if ( !$connection ) {
  echo 'Unable to connect with database ';
} else { 
  $query = "SELECT user.ID, user.FIRSTNAME, user.LASTNAME, user.EMAIL, user.PHONE_NUMBER, user.PARENT_NAME, user.STUDENT_INDEX, user.DATE_OF_ENTRY,course.COURSE_NAME, course.COURSE_ESPB, user.BALANCE FROM fr_users user INNER JOIN fr_courses course ON user.COURSE_ID WHERE USERNAME = '" . $_SESSION['username'] . "';";
  $result = mysqli_query($connection, $query);
  while($user_info = mysqli_fetch_row($result)){
    $userid = $user_info[0];
    $firstName = ucfirst($user_info[1]); 
    $lastName = ucfirst($user_info[2]);
    $email = ucfirst($user_info[3]);
    $phoneNumber = ucfirst($user_info[4]);
    $parentName = ucfirst($user_info[5]);
    $index = ucfirst($user_info[6]);
    $dateOfEntry = $user_info[7];
    $courseName = $user_info[8];
    $courseESPB = $user_info[9];
    $balance = $user_info[10];
  }

  $query = "SELECT MESSAGE_CONTENT FROM fr_user_upload_activity WHERE USER_ID = " . $userid;
  $result = mysqli_query($connection, $query);
  $messages = [];
  while($content = mysqli_fetch_row($result)){
    $messages[] = $content[0];
  }

}

?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <title>Student</title>
  </head>
  <body class="bg-light">

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <img src="../images/logo.svg" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
  </a>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home Page<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="./student/exams.php">Exams</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="./student/exam-registration.php">Exam Registration</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="./student/registered-exams.php">Registered Exams</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="./student/upload-docs.php">Upload Documentation</a>
      </li>
    </ul>
    <div class="navbar-nav nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <?php if(isset($_SESSION['username'])){echo ucfirst($_SESSION['username']);}; ?>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="logout.php">Logout</a>
        </div>
    </div>
  </div>
</nav>


<div class="container pt-3">
  <div class="row">
      
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <div class="jumbotron">
          <h2 class="display-5 text-center"><?php echo $firstName . " (" . $parentName . ") " . $lastName ; ?></h1>
          <br>
          <p class="lead">
            Student ID: <span class="badge badge-primary"><?php echo $index; ?></span>
          </p>
          <hr class="my-4">
          <p>
          <span class="media text-muted">Email:</span><?php echo $email; ?> 
          </p>
          <p> 
          <span class="media text-muted">Phone Number:</span><?php echo $phoneNumber; ?>
          </p>
          <p> 
          <span class="media text-muted">Balance:</span><span class="badge badge-success"><?php echo $balance . " RSD"; ?></span>
          </p>
        </div>          
     </div>
      
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <table class="table table-striped table-secondary">
            <tbody>
              <tr>
                <td class="text-center font-weight-bold">Academic Studie</td>
                <td class="text-center"><?php echo $courseName;?></td>
              </tr>
              <tr>
                <td class="text-center font-weight-bold">Academic Studie ESPB points</td>
                <td class="text-center"><?php echo $courseESPB;?></td>
              </tr>
              <tr>
                <td class="text-center font-weight-bold">Date of entry</td>
                <td class="text-center"><?php echo $dateOfEntry;?></td>
              </tr>
            </tbody>
          </table>

          <h3 class='text-center'></span><span class="badge badge-primary">User Activity</span></h3>
          <div class="overflow-auto" style="height: 300px">
              <?php
                $count = 0; 
                foreach($messages as $message){
                  echo '<div class="alert alert-info" role="alert">' . ++$count . " - " .  $message . "</div>";
                }
              ?>
          </div>

      </div>
        
  
  </div>
  
  </div>
  

</div>

<footer class="page-footer fixed-bottom font-small bg-dark ">   
    <div class="container">
        <div class="text-light footer-copyright text-center py-3">© 2020 Copyright:
            <a href="https://facebook.com/"> Freshman.com</a>
        </div>
    </div>
</footer>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
  </body>
</html>