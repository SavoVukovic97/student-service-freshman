
<?php 
session_start();
// Allow only admins to access this page.
if($_SESSION['userType'] != 1){
  die();
}

$DATABASE_HOST = 'localhost';
$DATABASE_USER = 'root';
$DATABASE_PASS = '';
$DATABASE_NAME = 'freshman';

$has_errors = 0;
$ERROR ="" ;

$con = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);

if ( !$con ) {
 
  echo 'Unable to connect with database ';
}

else{
  if (isset($_GET['edit'])) {
    $id = $_GET['edit'];
    $record = mysqli_query($con, "SELECT * FROM fr_courses WHERE COURSE_ID=$id");
    $resrows=mysqli_num_rows( $record);

    if ($resrows == 1 ) {
      $n = mysqli_fetch_array($record);
      $name = $n['COURSE_NAME'];
      $espb = $n['COURSE_ESPB'];
    }
  }
  else{
    die("COURSE NOT FOUND");
  }
if (isset($_POST['update'])) {
  
  $name = $_POST['name'];
  $espb = $_POST['num_espb'];
  mysqli_query($con, "UPDATE fr_courses SET COURSE_NAME='$name', COURSE_ESPB='$espb' WHERE COURSE_ID=$id");
  $_SESSION['message'] = "Course updated!"; 
 
}}?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Admin</title>
    <link rel="stylesheet" href="../css/editCourse.css">
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
  </head>
  <body>

<!--FOR ADDING COURSE-->

<div class="content2">
      <div class="login">
      <div class="logo">
        <img src="../images/logo.svg" style="width: 30%;">
      </div>
      <h1>Edit course</h1>
      <p class="error" <?php if($has_errors) {echo 'style="border:1.5px solid red;"';} ?>><?php if($has_errors) {echo $ERROR ;} ?></p>
      <?php if (isset($_SESSION['message'])): ?>
  <div class="msg">
    <?php 
      echo $_SESSION['message']; 
      unset($_SESSION['message']);
    ?>
  </div>
<?php endif ?>
      <form method="post">
        <div class="grid">

        <div class= "label">
          <label for="name">
            <i class="fas fa-graduation-cap"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="name" placeholder="Name of course"
          value="<?php echo $name; ?>"  required>
         </div> 
             
         <div class= "label">
          <label for="number">
            <i class="fas fa-book"></i>
          </label>
        </div>
        <div class="input">
          <input type="text" name="num_espb" placeholder="Number of ESPB" 
          value="<?php echo $espb; ?>" required>
         </div> 

        
        
</div>
        <input type="submit" name="update" value="Update">

      </form>
    </div>
</div>
<a href="admin.php">Go back to admin page</a>
</body>
</html>